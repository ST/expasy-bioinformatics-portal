# ---------------------------------------------------------------------------
# Nuxt
# ---------------------------------------------------------------------------

server {
    listen 8080;

    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    charset utf-8;

    location / {
        proxy_redirect                      off;
        proxy_set_header X-Real-IP          $remote_addr;
        proxy_set_header X-Forwarded-For    $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto  $scheme;
        proxy_read_timeout                  1m;
        proxy_connect_timeout               1m;

        # Websocket support
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;

        # Proxy to the Node instance of the client app
        proxy_pass http://node:3000;
    }

    # Proxy all API requests
    location ~ ^/(storage|nova-api|nova-vendor|_debugbar|vendor|api|admin)/? {
        proxy_pass http://localhost:81;

        proxy_redirect                      off;
        proxy_set_header Host $host;
    }
}

# ---------------------------------------------------------------------------
# Laravel
# ---------------------------------------------------------------------------

server {
    listen 81;

    index index.php index.html;
    root /var/www/api/public;
    charset utf-8;

    client_max_body_size 20m;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    error_page 404 /index.php;

    # Handle all php files (which will always be just /index.php)
    # via factcgi PHP-FPM unix socket
    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass php:9000;
        fastcgi_index index.php;
        # For comfortable debugging
        fastcgi_read_timeout 1000;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root/$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }

    location ~ /\.(?!well-known).* {
        deny all;
    }
}
