<?php

namespace Deployer;
use Symfony\Component\Dotenv\Dotenv;

use Symfony\Component\Console\Input\InputOption;

require_once __DIR__ . '/vendor/deployer/deployer/recipe/common.php';
require_once __DIR__ . '/vendor/deployer/deployer/contrib/slack.php';
require_once __DIR__ . '/recipe/laravel.php';
require_once __DIR__ . '/recipe/nuxt.php';

$slackWebHookUrl = null;

if ($slackWebHookUrl !== null) {
    set('slack_webhook', $slackWebHookUrl);
}

set('application', 'Expasy');

// keep the most recent 5 releases
set('keep_releases', 5);

set('shared_files', ['api/.env', 'client/.env']);

// Project repository
option('gitlab_token', null, InputOption::VALUE_OPTIONAL, 'gitlab_token to deploy.');
set('repository', static function () {
    $gitlab_token = '';
    if (input()->hasOption('gitlab_token')) {
        $gitlab_token = input()->getOption('gitlab_token');
        return 'https://' . $gitlab_token . '@gitlab.sib.swiss/ST/expasy-bioinformatics-portal.git';
    }

    return 'git@gitlab.sib.swiss:ST/expasy-bioinformatics-portal.git';
});

// Allocate tty for git clone. Default value is false.
set('git_tty', false);

// Share anonymous stats to repo owner
set('allow_anonymous_stats', false);

host('expasy-staging8.ulz.vital-it.ch')
    ->set('port', '22')
    ->set('stage', 'staging')
    ->set('deploy_path', '/var/www/html');

host('expasy-prod8.vital-it.ch')
    ->set('port', '22')
    ->set('stage', 'production')
    ->set('deploy_path', '/var/www/html');

set('deploy_path', '/var/www/html');
set('release_or_current_path', '{{ release_path }}/api');


desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'laravel:deploy-nova-credentials',
    'deploy:vendors',
    'laravel:nova-publish',
    'deploy:writable',
    'artisan:migrate',
    'artisan:storage:link',
    'artisan:view:cache',
    'artisan:config:cache',
    'nuxt:npm-ci',
    'nuxt:npm-build',
    'deploy:symlink',
    'nuxt:pm2-restart',
    'deploy:unlock',
    'deploy:cleanup',
    'deploy:success',
]);

$afterDeployFailed = ['deploy:unlock'];

if ($slackWebHookUrl !== null) {
    $afterDeployFailed[] = 'slack:notify:failure';
    before('deploy', 'slack:notify');
    after('success', 'slack:notify:success');
}

task('after:deploy:failed', $afterDeployFailed);
after('deploy:failed', 'after:deploy:failed');
