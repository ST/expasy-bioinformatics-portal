#!/bin/bash

# create dump of DB on PROD server
echo -e "DUMPING THE DATABASE FROM PROD"
ssh webdev@expasy-prod8.ulz.vital-it.ch -t "mysqldump --add-drop-table  expasy_prod > ~/prod_expasy.sql" # the credentials are stored in .my.cnf on the server

# # copy the dump locally
echo -e "COPY THE DUMP FROM PROD TO LOCAL"
scp webdev@expasy-prod8.ulz.vital-it.ch:~/prod_expasy.sql ~/

#restore DB
echo -e "IMPORTING THE DATABASE INTO LOCAL"
ddev import-db --file=~/prod_expasy.sql --database=db

#scout
ddev artisan scout:import "App\Models\Resource"

rsync -avz --delete webdev@expasy-prod8.ulz.vital-it.ch:/var/www/html/current/api/storage/app/public/ ./storage/app/public/

## sync local to dev
#scp  ~/prod_expasy.sql expasy-staging8.ulz.vital-it.ch:/home/webdev/prod_expasy.sql
# ssh expasy-staging8.ulz.vital-it.ch -t 'mysql  -e "DROP DATABASE IF EXISTS \`expasy_dev\`; CREATE DATABASE \`expasy_dev\`; USE \`expasy_dev\`; SOURCE /home/webdev/prod_expasy.sql ;"'




#  ./vendor/bin/dep deploy expasy-staging8.ulz.vital-it.ch --branch=develop2 --gitlab_token=glpat-Tq3szv9yetGyQhQ_7Gyg -v  -o release_name=1



# scp  ~/prod_expasy.sql expasy-prod8.vital-it.ch:/home/webdev/prod_expasy.sql
# ssh expasy-prod8.vital-it.ch -t 'mysql  -e "DROP DATABASE IF EXISTS \`expasy_prod\`; CREATE DATABASE \`expasy_prod\`; USE \`expasy_prod\`; SOURCE /home/webdev/prod_expasy.sql ;"'
