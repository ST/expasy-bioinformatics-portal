import vSelect from 'vue-select';

Nova.booting((Vue, router, store) => {
  Vue.component('index-enum', require('./components/IndexField'))
  Vue.component('detail-enum', require('./components/DetailField'))
  Vue.component('form-enum', require('./components/FormField'))
  Vue.component('v-select', vSelect);
})
