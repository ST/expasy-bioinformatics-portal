<?php

namespace Expasy\Enum;

use Laravel\Nova\Fields\Field;
use Laravel\Nova\Http\Requests\NovaRequest;

class Enum extends Field
{
    public $component = 'enum';

    protected function resolveAttribute($resource, $attribute)
    {
        // We always want to return an array of Enums for the front
        $realAttribute = $attribute.'s';

        return $resource->$realAttribute;
    }

    protected function fillAttributeFromRequest(NovaRequest $request, $requestAttribute, $model, $attribute)
    {
        if (! $request->exists($requestAttribute)) {
            return;
        }

        $requestData = json_decode($request->get($requestAttribute), true, 512, JSON_THROW_ON_ERROR);

        $enums = collect($requestData)->map(static fn ($enum) => $model->enumCasts[$attribute]::getInstance($enum['value']))->toArray();
        $model->{$attribute} = $model->enumCasts[$attribute]::flags($enums)->value;
    }

    public function resourceTypes(array $types)
    {
        $formattedTypes = collect($types)
            ->filter(static fn (\BenSampo\Enum\Enum $type) => $type->value !== 0)
            ->map(static fn (\BenSampo\Enum\Enum $type) => (array) $type)
            ->values()
            ->toArray();

        return $this->withMeta([
            'types' => $formattedTypes,
        ]);
    }
}
