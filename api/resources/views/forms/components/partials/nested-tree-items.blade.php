@foreach ($items as $item)
<li>
    <div class="flex items-center">

        <span x-on:click="opens[{{ $item->id }}] = !opens[{{ $item->id }}]" class="cursor-pointer"> X </span>

        <label class="cursor-pointer">
            <input
                type="checkbox"
                x-model="state"
                :value="{{ $item->id }}"
                class="mr-2"
            />
            {{ $item->name }} [{{ $item->id }} - {{ count($item->children) }} ]
        </label>
    </div>

    @if ($item->children->isNotEmpty())
        <ul x-show="opens[{{ $item->id }}]"
                style="padding-left: 15px;"
                class="pl-4 ml-2 border-l">
            @include('forms.components.partials.nested-tree-items', ['items' => $item->children])
        </ul>
    @endif
</li>s
@endforeach