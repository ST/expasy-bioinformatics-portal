<x-dynamic-component
    :component="$getFieldWrapperView()"
    :field="$field"
>
    <div x-data="{ state: $wire.$entangle('{{ $getStatePath() }}'), opens: []  }">



        <textarea x-model="state" cols="30" rows="10"></textarea>
        <ul>
            {{-- @include('livewire.partials.nested-tree-items', ['items' => $getItems()]) --}}
            @include('forms.components.partials.nested-tree-items', ['items' => $getItems()])
            {{-- @include('partials.nested-tree-items', ['items' => $getItems()]) --}}
        </ul>

    </div>
</x-dynamic-component>
