@extends('layouts.app')




@section('content')
    <main role="main" class="page-main">


        @include('partials.search')

        <div class="container container--sm">
            <section class="error-page border-neutral-200 border-t">
                <h1 class="mb-5">
                    Seems that you are lost&nbsp;<span class="text-sm text-neutral-500">(404)</span></h1>
                <p class="text-neutral-700 mb-8">The page you are looking for doesn't exist.</p> <a href="/"
                    class="btn btn--primary nuxt-link-active"> Back to home </a>
            </section>
        </div>

    </main>
@endsection
