@extends('layouts.app')



@if (false)
    <section class="news-wrapper mb-4 news-wrapper--is-displayed">
        <div class="container">
            <article class="news">
                <div class="news__close"><svg focusable="false" aria-labelledby="idqqwb8pduv" role="img"
                        class="icon icon--close icon--100">
                        <title id="idqqwb8pduv">Hide the news</title>
                        <use xlink:href="/_nuxt/img/icons.2l2yMrcx.svg#close"></use>
                    </svg></div>
                <div class="news__container">
                    <figure class="news__figure"><!----> <a href="https://www.uniprot.org/" target="_blank"
                            rel="noopener noreferrer"><img
                                src="/storage/attachments/NAcGsl7ctIcID6gOIS5Ud2tkKpdGVm5Pp6sOfniB.png"
                                alt=""></a></figure>
                    <div class="news__content"><span class="text-neutral-500">
                            Posted 29 November 2024 -
                        </span>
                        <div class="news__body">
                            <p><a target="_self" href="https://www.expasy.org/resources/enzchemred " tt-mode="url"
                                    title="EnzChemRED"><strong>EnzChemRED</strong></a> is a new training and
                                benchmarking dataset to support the development of Natural Language Processing (NLP)
                                methods such as large language models (LLMs) that can assist enzyme curation. Use it
                                to
                                boost your enzyme annotation!&nbsp;</p>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
@endif




@section('content')
    <main role="main" class="page-main" x-data="{ categories: [] }">


        @include('partials.search')




        <div class="container">
            <div class="flex-grid flex-grid--md">

                <section class="md:w-1/3 lg:w-1/4 xl:w-1/5 flex-grid__item w-full">
                    <ul>
                        @foreach (App\Models\ResourceCategory::where('parent_id', null)->get() as $resourceCategory)
                            <li class="p-2 border-b-2">
                                <label>
                                    <input type="checkbox" x-model="categories" value="{{ $resourceCategory->id }}">
                                    {{ $resourceCategory->title }}
                                </label>

                                @if ($resourceCategory->children->count() > 0)
                                    <ul class="pl-4">
                                        @foreach ($resourceCategory->children as $child)
                                            <li>
                                                <label>
                                                    <input type="checkbox" x-model="categories" value="{{ $child->id }}">
                                                    {{ $child->title }}
                                                </label>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </section>

                <section class="md:w-2/3 lg:w-3/4 xl:w-4/5 flex-grid__item w-full">

                    <section class="py-5 border-neutral-200 border-t">
                        <h2 class="h3 mb-5 inline-block"> SIB Resources </h2>


                        <div class="flex gap-4">
                            @foreach (App\Models\Resource::basicQuery()->where('pinned', 1)->get() as $resource)
                                <div x-show="categories.length == 0 || categories.some(el => {{ $resource->categories->pluck('id') }}.map(String).includes(String(el)))"
                                    class="border-2 p-4">
                                    <h3>{{ $resource->title }}</h3>
                                    <p>{{ $resource->short_description }}</p>
                                </div>
                            @endforeach
                        </div>
                    </section>

                    <section class="py-5 border-neutral-200 border-t">
                        <h2 class="h3 mb-5 inline-block"> Other Resources of SIB Groups </h2>


                        <div class="flex gap-4">
                            @foreach (App\Models\Resource::basicQuery()->where('pinned', 0)->get() as $resource)
                                <div x-show="categories.length == 0 || categories.some(el => {{ $resource->categories->pluck('id') }}.map(String).includes(String(el)))"
                                    class="border-2 p-4">
                                    <h3>{{ $resource->title }}</h3>
                                    <p>{{ $resource->short_description }}</p>
                                </div>
                            @endforeach
                        </div>
                    </section>
                </section>
            </div>
        </div>
    </main>
@endsection
