<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Expasy</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />


    @vite(['resources/css/app.css', 'resources/js/app.js'])
    @livewireStyles



    @if (app()->isProduction())
        <!-- Matomo -->
        <script>
            var _paq = window._paq = window._paq || [];
            /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function() {
                var u = "//matomo.sib.swiss/";
                _paq.push(['setTrackerUrl', u + 'matomo.php']);
                _paq.push(['setSiteId', '2']);
                var d = document,
                    g = d.createElement('script'),
                    s = d.getElementsByTagName('script')[0];
                g.async = true;
                g.src = u + 'matomo.js';
                s.parentNode.insertBefore(g, s);
            })();
        </script>
        <!-- End Matomo Code -->
    @endif


</head>

<body>


    <header role="banner" class="mb-4">
        <div class="flex max-sm:flex-col container mx-auto">
            <a href="/" aria-current="page"
                class="flex-1 max-sm:hidden nuxt-link-exact-active nuxt-link-active">
                <div class="pt-4">
                    <img src="/img/sib.png" srcset="/img/sib.png, /img/sib@2x.png 2x"
                        alt="SIB: Swiss Institute of Bioinformatics" class="inline-block"></div>
            </a>
            <nav role="navigation" class="max-sm:w-full">
                <ul class="nav">
                    <li class="inline-block">
                        <a href="{{ route('home') }}" aria-current="page"
                            class="nav__link {{ Route::currentRouteName() === 'home' ? 'active' : '' }}"> Home </a>
                    </li>
                    <li class="inline-block"><a href="{{ route('about') }}" class="nav__link {{ Route::currentRouteName() === 'about' ? 'active' : '' }}""> About </a></li>
                    <li class="inline-block"><a href="https://www.sib.swiss/about/news" target="_blank"
                            rel="noopener noreferer" class="nav__link">
                            SIB News
                        </a></li>
                    <li class="inline-block"><a href="{{ route('contact') }}" class="nav__link {{ Route::currentRouteName() === 'contact' ? 'active' : '' }}""> Contact </a></li>
                </ul>
            </nav>
        </div>
    </header>

    @if (false)
        <section class="news-wrapper mb-4 news-wrapper--is-displayed">
            <div class="container">
                <article class="news">
                    <div class="news__close"><svg focusable="false" aria-labelledby="idqqwb8pduv" role="img"
                            class="icon icon--close icon--100">
                            <title id="idqqwb8pduv">Hide the news</title>
                            <use xlink:href="/_nuxt/img/icons.2l2yMrcx.svg#close"></use>
                        </svg></div>
                    <div class="news__container">
                        <figure class="news__figure"><!----> <a href="https://www.uniprot.org/" target="_blank"
                                rel="noopener noreferrer"><img
                                    src="/storage/attachments/NAcGsl7ctIcID6gOIS5Ud2tkKpdGVm5Pp6sOfniB.png"
                                    alt=""></a></figure>
                        <div class="news__content"><span class="text-neutral-500">
                                Posted 29 November 2024 -
                            </span>
                            <div class="news__body">
                                <p><a target="_self" href="https://www.expasy.org/resources/enzchemred " tt-mode="url"
                                        title="EnzChemRED"><strong>EnzChemRED</strong></a> is a new training and
                                    benchmarking dataset to support the development of Natural Language Processing (NLP)
                                    methods such as large language models (LLMs) that can assist enzyme curation. Use it
                                    to
                                    boost your enzyme annotation!&nbsp;</p>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </section>
    @endif



    @yield('content')




    <footer role="contentinfo" class="text-sm text-neutral-700 max-sm:text-center sm:mt-12 pt-8 pb-8 bg-neutral-100">
        <div class="container">
            <div class="flex max-sm:flex-col sm:items-center">
                <div class="md:flex-1 mb-8 md:mb-0">
                    <div class="max-w-sm md:max-w-3xl md:pr-12">

                        <svg aria-hidden="true" focusable="false" role="img"
                            class="md:mr-2 mb-2 inline-block icon icon--sib-mark icon--sib-mark">
                            <use xlink:href="/img/icons/sib-mark.svg"></use>
                        </svg>

                        <div class="md:inline-block">
                            Expasy is operated by the
                            <a href="https://sib.swiss" target="blank" rel="noopener noreferer">SIB Swiss Institute of
                                Bioinformatics</a>
                            |
                            <a href="{{ route('terms') }}" class=""> Terms of Use </a>
                        </div>
                    </div>
                </div> <a href="#top"> Back to the top </a>
            </div>
        </div>
    </footer>



    @livewireScripts
</body>

</html>
