@extends('layouts.app')




@section('content')
    <main role="main" class="page-main flex-grow">



        <div class="flex flex-col h-full">
            <div class="flex-grow">
                <chat-with-context chat-endpoint="https://chat.expasy.org/chat"
                    feedback-endpoint="https://chat.expasy.org/feedback" api-key="a**2!seUKJnZG!ka6koZqS"
                    examples="Which resources are available at the SIB?,How can I get the HGNC symbol for the protein P68871?,What are the rat orthologs of the human TP53?,Where is expressed the gene ACE2 in human?,Anatomical entities where the INS zebrafish gene is expressed and its gene GO annotations,List the genes in primates orthologous to genes expressed in the fruit fly eye"></chat-with-context>
            </div>
        </div>


    </main>
@endsection
