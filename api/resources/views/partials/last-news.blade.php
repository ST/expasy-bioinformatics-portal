<section class="news-wrapper mb-4 news-wrapper--is-displayed">
    <div class="container">
        <article class="news">
            <div class="news__close"><svg focusable="false" aria-labelledby="id5q44jwvul" role="img"
                    class="icon icon--close icon--100">
                    <title id="id5q44jwvul">Hide the news</title>
                    <use xlink:href="/_nuxt/img/icons.2l2yMrcx.svg#close"></use>
                </svg></div>
            <div class="news__container">
                <figure class="news__figure">
                    <a href="{{ $lastNews->image_url }}" target="_blank" rel="noopener noreferrer"><img
                            src="/storage/{{ $lastNews->image_path }}" alt=""></a>
                </figure>
                <div class="news__content"><span class="text-neutral-500">
                        Posted {{ $lastNews->published_from->format('d M Y') }} -
                    </span>
                    <div class="news__body">
                        {!! $lastNews->body !!}
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>
