<div class="container container--xs">
    <form action="/search" method="get" class="search">
        <div class="text-center"><img src="/images/expasy-homepage.png"
                srcset="/images/expasy-homepage.png, /images/expasy-homepage@2x.png 2x"
                alt="Expasy: Swiss Bioinformatics Resource Portal" class="mb-8 inline-block"></div>
        <div class="flex mb-1"><label for="search" class="inline-block h4 mb-2 sr-only">
                Explore high-quality biological data resources
            </label> <input id="search" name="search" type="search" required="required"
                class="field search__input"> <button type="submit" class="btn search__submit"><svg aria-hidden="true"
                    focusable="false" role="img" class="icon icon--search icon--150"><!---->
                    <use xlink:href="/_nuxt/img/icons.2l2yMrcx.svg#search"></use>
                </svg></button></div>
        <div class="text-sm">
            e.g.
            <a href="{{ route('search', 'blast') }}" class="">BLAST</a>,
            <a href="{{ route('search', 'uniprot') }}" class="">UniProt</a>,
            <a href="{{ route('search', 'msh6') }}" class="">MSH6</a>,
            <a href="{{ route('search', 'albumin') }}" class="">Albumin</a>...
        </div> <!---->
    </form>
</div>
