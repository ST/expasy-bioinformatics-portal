@extends('layouts.app')




@section('content')
    <main role="main" class="page-main">


        @include('partials.search')



        <div class="container container--md">
            <section class="py-5 border-neutral-200 border-t">
                <div class="rich-text">
                    <h1>About Expasy</h1>
                    <p>
                        Expasy is the bioinformatics resource portal of the SIB Swiss Institute of Bioinformatics (<a
                            href="#some-history">more about its history</a>).
                    </p>
                    <p>
                        It is an extensible and integrative portal which provides access to over 160 databases and software
                        tools,
                        developed by SIB Groups and supporting a range of life science and clinical research domains, from
                        genomics,
                        proteomics and structural biology, to evolution and phylogeny, systems biology and medical
                        chemistry.
                    </p>
                    <h2>The Expasy search engine</h2>
                    <p>
                        Thanks to a user-friendly search engine, Expasy allows you to seamlessly 1) query in parallel a
                        subset of SIB
                        databases through a single search, and to 2) surface related information and knowledge from the
                        complete set
                        of &gt;160 resources on the portal. Expasy provides information that is automatically aligned with
                        the most
                        recent release of each resources, thereby ensuring up-to-date information.
                    </p>
                    <p>
                        The terms used in Expasy are based on the
                        <a href="http://edamontology.org" target="_blank" rel="noreferer noopener">EDAM ontology</a>
                        (Black et al., F1000Research., 2022;
                        <a href="https://doi.org/10.7490/f1000research.1118900.1" target="_blank" rel="noreferer noopener">
                            DOI: 10.7490/f1000research.1118900.1</a>)
                    </p>
                    <h2>Contact</h2>
                    <p>
                        Please contact the
                        <a href="/contact" class=""> Expasy Helpdesk </a>
                        if you experience any issues – or simply to let us know what a difference the portal made for your
                        purpose!
                    </p>
                    <h2>Credit</h2>
                    <p>We do hope your use of Expasy helped you in your research or educational activities!</p>
                    <p style="margin-left: 30px;">
                        Séverine Duvaud, Chiara Gabella, Frédérique Lisacek, Heinz Stockinger, Vassilios Ioannidis,
                        Christine Durinx;
                        <span style="font-weight: bold;">Expasy, the Swiss Bioinformatics Resource Portal, as designed by
                            its users</span><br>
                        Nucleic Acids Research, 2021.
                        <a href="https://doi.org/10.1093/nar/gkab225" target="_blank" rel="noreferer noopener">DOI:
                            10.1093/nar/gkab225</a>
                    </p>
                    <h2 id="some-history">Some history</h2>
                    <p>
                        Expasy was created in August 1993 - the dawn of the internet era. At that time, it was referred to
                        as 'ExPASy,
                        the Expert Protein Analysis System' as proteins were its primary focus. It was the first life
                        science website
                        - and among the 150 very first websites in the world!
                    </p>
                    <p>
                        In June 2011, it became the SIB Expasy Bioformatics Resources Portal: a diverse catalogue of
                        bioinformatics
                        resources developed by SIB Groups.
                    </p>
                    <p>
                        The current version of Expasy was released in October 2020 following a massive user study and taking
                        into
                        account design, user experience and architecture aspects: we thank all participants for their help
                        in shaping
                        Expasy 3.0!
                    </p>
                </div>
            </section>
            <section class="mt-5 py-5 border-neutral-200 border-t">
                <h2 class="h3 mb-5"> Links and documentation </h2>
                <div class="flex-grid flex-grid--multiline">
                    <div class="md:w-1/2 flex-grid__item w-full">
                        <h3 class="h4 mb-5"> SIB Activities </h3>
                        <div class="rich-text">
                            <ul>
                                <li><a href="https://www.sib.swiss/database-software-tools/sib-resources" target="_blank"
                                        rel="noreferer noopener">SIB Resources</a>: set of tools and databases by SIB Groups
                                    that are deemed of particular importance to the life-science
                                    community.
                                </li>
                                <li><a href="https://www.sib.swiss/services/portfolio" target="_blank"
                                        rel="noreferer noopener">Center of excellence</a>: in-depth expertise and support in
                                    bioinformatics, from secure infrastructure for sensitive data and
                                    analyses of all kinds of biological data to software development and data management.
                                </li>
                                <li><a href="https://www.sib.swiss/training/upcoming-training-courses" target="_blank"
                                        rel="noreferer noopener">Training</a>: large portfolio of bioinformatics courses and
                                    workshops for researchers and healthcare professionals,
                                    in the academia and the industry.
                                </li>
                                <li><a href="https://www.sib.swiss/services/portfolio" target="_blank"
                                        rel="noreferer noopener">Let’s collaborate</a>: supporting your needs with one-off
                                    services up as well as long-term collaborative support.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="md:w-1/2 flex-grid__item w-full">
                        <h3 class="h4 mb-5"> Outreach material </h3>
                        <div class="rich-text">
                            <ul>
                                <li><a href="https://www.sib.swiss/what-is-bioinformatics#activities-around-bioinformatics"
                                        target="_blank">Bioinformatics for all</a></li>
                                <li><a href="https://www.chromosomewalk.ch/en/list-of-chromosomes/"
                                        target="_blank">ChromosomeWalk</a>: a
                                    saunter along the human genome? Take a walk and discover the world of genes, proteins
                                    and
                                    bioinformatics!
                                </li>
                                <li><a href="https://www.precisionmed.ch/" target="_blank">PrecisionMedecine</a>: an
                                    overview of what
                                    precision medicine means in oncology.
                                </li>
                                <li><a href="http://www.drug-design-workshop.ch/" target="_blank">Drug Design Workshop</a>:
                                    a web-based
                                    educational tool to introduce computer-aided drug design to the general public.
                                </li>
                                <li><a href="https://genome-jumper.sib.swiss/" target="_blank">Genome Jumper</a>: explore
                                    human diversity
                                    with this educational mobile game available on Google Play and App Store (EN, FR, DE).
                                </li>
                                <li><a href="https://web.expasy.org/spotlight/" target="_blank">Protein Spotlight</a>: an
                                    electronic
                                    magazine that introduces the tremendously varied world of proteins.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection
