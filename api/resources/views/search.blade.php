@extends('layouts.app')








@section('content')
    <main role="main" class="page-main">


        @include('partials.search')




        <div class="container"><!---->
            <section class="section-no-hit py-5 section-no-hit--is-hidden"><button class="framed">
                    You can also query "blast" into a selection of SIB databases in parallel
                    <span><svg focusable="false" aria-labelledby="idxh7iueeky" role="img" class="icon icon--plus icon--100">
                            <title id="idxh7iueeky">Hide Hits section</title>
                            <use xlink:href="/_nuxt/img/icons.2l2yMrcx.svg#plus"></use>
                        </svg></span></button></section>



            <section class="py-5 border-neutral-200 border-t"><button
                    class="framed mb-4"><strong>“uniprot”</strong>&nbsp;queried in 19 SIB databases
                    <span><svg focusable="false" aria-labelledby="idnr4to2j8n" role="img"
                            class="icon icon--minus icon--100">
                            <title id="idnr4to2j8n">Display Hits section</title>
                            <use xlink:href="/_nuxt/img/icons.2l2yMrcx.svg#minus"></use>
                        </svg></span></button>
                <div class="flex-grid flex-grid--even">
                    <div class="md:w-1/2 lg:w-1/4 flex-grid__item w-full">
                        <h3 class="search-page__title h4"><svg aria-hidden="true" focusable="false" role="img"
                                class="mr-2 text-primary-500 icon icon--stru-biology icon--125"><!---->
                                <use xlink:href="/_nuxt/img/icons.2l2yMrcx.svg#stru-biology"></use>
                            </svg>
                            Structural Biology
                        </h3>
                        <div class="mb-2" test="swiss_model">
                            <div class="hit__header flex">
                                <div class="flex-1">
                                    <h4 class="h5">
                                        SWISS-MODEL Repository
                                    </h4>
                                </div>
                                <div class="text-sm"><a href="https://swissmodel.expasy.org/repository?query=uniprot"
                                        target="_blank" rel="noopener noreferer">
                                        130
                                        hits</a></div>
                            </div>
                            <div class="text-sm text-neutral-700">
                                Protein structure homology models
                            </div>
                        </div>
                    </div>
                    <div class="md:w-1/2 lg:w-1/4 flex-grid__item w-full">
                        <h3 class="search-page__title h4"><svg aria-hidden="true" focusable="false" role="img"
                                class="mr-2 text-primary-500 icon icon--gene icon--125"><!---->
                                <use xlink:href="/_nuxt/img/icons.2l2yMrcx.svg#gene"></use>
                            </svg>
                            Genes &amp; Genomes
                        </h3>
                        <div class="mb-2 text-neutral-500" test="bgee">
                            <div class="hit__header flex">
                                <div class="flex-1">
                                    <h4 class="h5">
                                        Bgee
                                    </h4>
                                </div>
                                <div class="text-sm"> 0 hit </div>
                            </div>
                            <div class="text-sm text-neutral-700">
                                Gene expression expertise
                            </div>
                        </div>
                    </div>
                    <div class="lg:w-1/2 flex-grid__item w-full">
                        <div class="flex-grid flex-grid--even">
                            <div class="flex-grid__item w-full">
                                <h3 class="search-page__title h4"><svg aria-hidden="true" focusable="false"
                                        role="img" class="mr-2 text-primary-500 icon icon--protein icon--125"><!---->
                                        <use xlink:href="/_nuxt/img/icons.2l2yMrcx.svg#protein"></use>
                                    </svg>
                                    Proteins &amp; Proteomes
                                </h3>
                            </div>
                            <div class="md:w-1/2 flex-grid__item w-full">
                                <div class="mb-2" test="prosite">
                                    <div class="hit__header flex">
                                        <div class="flex-1">
                                            <h4 class="h5">
                                                PROSITE
                                            </h4>
                                        </div>
                                        <div class="text-sm"><a
                                                href="https://prosite.expasy.org/cgi-bin/prosite/prosite_search_full.pl?search=uniprot&amp;format=html"
                                                target="_blank" rel="noopener noreferer">
                                                50
                                                hits</a></div>
                                    </div>
                                    <div class="text-sm text-neutral-700">
                                        Protein family and domain database
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



            <section class="py-5 border-neutral-200 border-t">
                <h2 class="h3 mb-5"><span> {{ count($result['resources']) }}
                        {{ str('resource')->plural($result['resources']) }} found for </span> <span
                        class="font-bold">{{ $query }}</span></h2>
                <ul class="flex-grid flex-grid--multiline flex-grid--even">
                    <li class="sm:w-1/2 md:w-1/2 lg:w-1/3 xl:w-1/4 flex-grid__item w-full">
                        <article class="resource">
                            <div class="flex items-center mb-3">
                                <div class="flex-1 text-primary-500">
                                    <ul>
                                        <li class="inline-block mr-2 last:mr-0"><svg aria-hidden="true" focusable="false"
                                                role="img" class="icon icon--protein icon--125"><!---->
                                                <use xlink:href="/_nuxt/img/icons.2l2yMrcx.svg#protein"></use>
                                            </svg>
                                            <ul aria-label="Resource Categories">
                                                <li class="inline-block sr-only"><!---->
                                                    Proteins &amp; Proteomes
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="text-neutral-500">
                                    <ul>
                                        <li class="inline-block mr-2 last:mr-0"><svg aria-hidden="true" focusable="false"
                                                role="img" class="icon icon--db icon--125"><!---->
                                                <use xlink:href="/_nuxt/img/icons.2l2yMrcx.svg#db"></use>
                                            </svg>
                                            <ul aria-label="Resource Types">
                                                <li class="inline-block sr-only"><!---->
                                                    Database
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div> <a href="/resources/uniprotkb" class="resource__link">
                                <h3 class="h5">
                                    UniProtKB
                                </h3>
                            </a>
                            <div class="text-sm leading-snug mt-1">
                                Protein sequence database
                            </div>
                        </article>
                    </li>
                </ul>
            </section>

        </div>


    </main>
@endsection
