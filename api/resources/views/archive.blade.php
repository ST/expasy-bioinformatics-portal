@extends('layouts.app')




@section('content')
    <main role="main" class="page-main">


        @include('partials.search')



        @include('archives.' . $resourceSlug)


    </main>
@endsection
