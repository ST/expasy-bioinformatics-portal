{{-- @extends('nova::auth.layout') --}}

{{-- @section('content') --}}

{{-- @include('nova::auth.partials.header') --}}

<div class="bg-white shadow rounded-lg p-8 max-w-login mx-auto">
    {{ csrf_field() }}

    {{-- @component('nova::auth.partials.heading')
        {{ __('Welcome Back!') }}
    @endcomponent --}}

    @if (session('error'))
        <p class="text-center font-semibold text-danger my-3">
            Error: {{ session('error') }}
        </p>
    @endif

    <a href="{{ route('sso.redirect') }}" class="w-full btn btn-default btn-primary hover:bg-primary-dark text-center" type="submit">
        {{ __('Login') }}
    </a>
</div>
{{-- @endsection --}}
