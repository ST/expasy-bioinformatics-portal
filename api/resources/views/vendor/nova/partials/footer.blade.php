<p class="mt-8 text-center text-xs text-80">
    <a href="https://www.expasy.org" class="text-primary dim no-underline">Expasy</a>
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }}
</p>
