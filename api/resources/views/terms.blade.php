@extends('layouts.app')




@section('content')
    <main role="main" class="page-main">


        @include('partials.search')



        <div class="container container--md">
            <section class="py-5 border-neutral-200 border-t">
                <div class="rich-text">
                    <h1>Terms of Use</h1>
                    <p>
                        The SIB Swiss Institute of Bioinformatics (“SIB”) is committed to open research and open knowledge.
                        Through
                        its Expasy bioinformatics resource portal, SIB provides the national and international research
                        community with
                        access to over 160 databases and software tools (the “resources”). These resources are developed by
                        SIB Groups
                        and support a range of life science and clinical research domains (read more in “About Expasy”).
                    </p>
                    <p>Your access to, and use of, the Expasy website are subject to the following terms and conditions:</p>
                    <ol>
                        <li>
                            SIB imposes no other restriction on the use or redistributions of the resources referenced on
                            the Expasy
                            website than those provided by the owner of the dedicated resource. When using a resource in
                            your work, you
                            must comply with the terms of use of each resource and give the appropriate credits as per the
                            corresponding
                            copyright notice and good scientific practice. The name and logos of the SIB Swiss Institute of
                            Bioinformatics must not be associated with publicity or business promotion without SIB’s prior
                            written
                            approval.
                        </li>
                        <li>
                            The Expasy website and the resources are provided “as is” and “as available”. SIB does not
                            guarantee the
                            correctness, accuracy, reliability and completeness of any resources referenced on the Expasy
                            website, nor
                            the suitability for any specific purpose. SIB bears no responsibility for any direct, indirect
                            incidental or
                            consequential loss or damages, resulting from the use of the Expasy website. SIB bears no
                            responsibility for
                            the consequences of any temporary or permanent discontinuity of the Expasy website.
                        </li>
                        <li>
                            Through its <a href="https://www.sib.swiss/privacy-policy" target="_blank">Privacy Policy</a>, SIB
                            is
                            committed to ensuring your privacy and the confidentiality of your personal data. When accessing
                            and using
                            the Expasy website, SIB only collects the following personal data: IP addresses, date and time
                            of a visit,
                            operating system, browser, email, address and content of your queries (only when you send an
                            email to SIB).
                            Your personal data is only used to provide you with access to the Expasy service, to ensure
                            compliance with
                            the terms of use, to create anonymous usage statistics, and if needed, to answer a request that
                            you might
                            send to SIB. When you access to and use a resource referenced on the Expasy website, please
                            refer to each
                            applicable privacy policy prior to sharing your personal data.
                        </li>
                        <li>
                            If you have any questions about these terms of use, you can contact the SIB Legal and Technology
                            Transfer
                            Office at <a
                                onclick="window.open(atob('bWFpbHRvOmxlZ2FsQHNpYi5zd2lzcw=='), '_blank')">legal@sib.swiss</a>.
                            We reserve the right to amend and update these Terms of use at any time.
                        </li>
                        <li>
                            These terms of use and the use of the Expasy website are governed by Swiss substantive law,
                            without
                            reference to its conflict of laws provisions. The competent courts in Lausanne, Switzerland,
                            have exclusive
                            jurisdiction.
                        </li>
                    </ol>
                    <p class="italic">Last revised: 1 July 2020</p>
                </div>
            </section>
        </div>

    </main>
@endsection
