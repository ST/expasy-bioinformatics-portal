@extends('layouts.app')




@section('content')
    <main role="main" class="page-main">


        @include('partials.search')



        <div class="container container--md">
            <section class="py-5 border-neutral-200 border-t">
                <div class="rich-text">
                    <h1>Contact - Expasy Helpdesk</h1>
                    <p>
                        If you have questions or comments regarding one of the resources hosted on this portal, please use
                        the support
                        and contact options provided by that specific resource.
                    </p>
                    <p>
                        For general questions concerning the Expasy portal, please contact the
                        <a onclick="window.open(atob('bWFpbHRvOmhlbHBkZXNrQGV4cGFzeS5vcmc='), '_blank')">Expasy Helpdesk</a>.
                    </p>
                </div>
            </section>
        </div>

    </main>
@endsection
