<?php

use App\Http\Controllers\AboutChatController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\ArchiveController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\TermsController;
use Illuminate\Support\Facades\Route;

Route::get('/admin/login', 'Nova\LoginController@showLoginForm');
Route::get('/admin/login/sso', 'Nova\LoginController@redirectToProvider')->name('sso.redirect');
Route::get('/admin/login/sso/callback', 'Nova\LoginController@handleProviderCallback');

Route::get('/home2', [HomeController::class, 'index'])->name('home');
Route::get('/search2/{word}', [SearchController::class, 'index'])->name('search');
Route::get('/about2', [AboutController::class, 'index'])->name('about');
Route::get('/contact2', [ContactController::class, 'index'])->name('contact');
Route::get('/terms-of-use2', [TermsController::class, 'index'])->name('terms');
Route::get('/about-chat2', [AboutChatController::class, 'index'])->name('about-chat');
Route::get('/chat2', [ChatController::class, 'index'])->name('chat');
Route::get('/archives2/{resourceSlug}', [ArchiveController::class, 'index'])->name('archive');

Route::post('/logout', 'LoginController@logout')->name('admin2.logout');
Route::get('/login', 'LoginController@redirectToProvider')->name('admin2.login');
Route::get('/login/sso/callback', 'LoginController@handleProviderCallback')->name('admin2.sso.redirect');
