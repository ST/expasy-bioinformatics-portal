<?php

use App\Http\Controllers\Api\V1\NewsController;
use App\Http\Controllers\Api\V1\SearchController;
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'V1', 'prefix' => 'v1'], function () {

    Route::get('/news/last', [NewsController::class, 'last']);

    Route::get('/resources/search', [SearchController::class, 'search'])->name('search');
    Route::get('/resources/{resource}/search', [SearchController::class, 'metaSearch']);
    Route::get('/resources/search-meilisearch', [SearchController::class, 'searchMeilisearch'])->name('searchMeilisearch');

    Route::resource('resources', 'ResourceController')
        ->only([
            'show',
            'index',
        ]);

    Route::resource('resources-filters', 'ResourceFilterController')->only([
        'index',
    ]);

});

Route::fallback(function () {
    return App::call('\App\Http\Controllers\Api\ApiController@renderNotFound', ['message' => 'Not found']);
});

Route::get('speed-test', function () {
    return response('Test page', 200)
        ->header('Content-Type', 'text/html');
});
