<?php

namespace Tests\Unit;

use App\Enums\ExpasyQueryType;
use App\Libraries\DBResourceSearchQuery;
use Tests\TestCase;

class SearchQueryResolverTest extends TestCase
{
    /**
     * @dataProvider provideQueryExamples
     */
    public function test_resolver($expectedResult, $query)
    {
        $dbResourceSearchQuery = new DBResourceSearchQuery;
        $queryType = $dbResourceSearchQuery->resolveQueryType($query);
        $this->assertEquals($expectedResult, $queryType);
    }

    public static function provideQueryExamples()
    {
        return [
            'P00750' => [ExpasyQueryType::UNIPROTAC(), 'P00750'],
            'P0CW05' => [ExpasyQueryType::UNIPROTAC(), 'P0CW05'],
            'Q545V4' => [ExpasyQueryType::UNIPROTAC(), 'Q545V4'],
            'A0A1U7R335' => [ExpasyQueryType::UNIPROTAC(), 'A0A1U7R335'],

            'AAXA_CHLCV' => [ExpasyQueryType::UNIPROTID(), 'AAXA_CHLCV'],
            'A4_HUMAN' => [ExpasyQueryType::UNIPROTID(), 'A4_HUMAN'],
            'A0A1U7R335_MESAU' => [ExpasyQueryType::UNIPROTID(), 'A0A1U7R335_MESAU'],
            'Q545V4_MOUSE' => [ExpasyQueryType::UNIPROTID(), 'Q545V4_MOUSE'],

            'UPI0000000013' => [ExpasyQueryType::UNIPARC(), 'UPI0000000013'],
            'UPI000000002D' => [ExpasyQueryType::UNIPARC(), 'UPI000000002D'],
            'UPI0000000064' => [ExpasyQueryType::UNIPARC(), 'UPI0000000064'],

            '2LU3' => [ExpasyQueryType::PDBID(), '2LU3'],
            '1b38' => [ExpasyQueryType::PDBID(), '1b38'],
            '4Y0G' => [ExpasyQueryType::PDBID(), '4Y0G'],
            '1pgp' => [ExpasyQueryType::PDBID(), '1pgp'],

            'WP_015411143' => [ExpasyQueryType::REFSEQ(), 'WP_015411143'],
            'ZP_00126997' => [ExpasyQueryType::REFSEQ(), 'ZP_00126997'],
            'NZ_NVQJ01000059.1' => [ExpasyQueryType::REFSEQ(), 'NZ_NVQJ01000059.1'],
            'NP_001271025.1' => [ExpasyQueryType::REFSEQ(), 'NP_001271025.1'],
            'NW_001085247.1' => [ExpasyQueryType::REFSEQ(), 'NW_001085247.1'],

            'ENSDART00000049368' => [ExpasyQueryType::ENSEMBLID(), 'ENSDART00000049368'],
            'ENSG00000141867' => [ExpasyQueryType::ENSEMBLID(), 'ENSG00000141867'],
            'ENSDARG00000004472' => [ExpasyQueryType::ENSEMBLID(), 'ENSDARG00000004472'],
            'ENST00000371835' => [ExpasyQueryType::ENSEMBLID(), 'ENST00000371835'],

            'FJI524850.1' => [ExpasyQueryType::GBA(), 'FJI524850.1'],

            '1.2.2.4' => [ExpasyQueryType::EC(), '1.2.2.4'],
            '3.2.2.4' => [ExpasyQueryType::EC(), '3.2.2.4'],
            '3.2.2.-' => [ExpasyQueryType::EC(), '3.2.2.-'],

            'keratin' => [null, 'keratin'],
            'duchenne disease' => [null, 'duchenne disease'],
            'tpa' => [null, 'tpa'],
            'bfl1' => [null, 'bfl1'],
        ];
    }
}
