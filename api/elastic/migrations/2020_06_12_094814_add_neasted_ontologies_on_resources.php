<?php

declare(strict_types=1);

use Elastic\Adapter\Indices\Mapping;
use Elastic\Migrations\Facades\Index;
use Elastic\Migrations\MigrationInterface;

final class AddNeastedOntologiesOnResources implements MigrationInterface
{
    /**
     * Run the migration.
     */
    public function up(): void
    {
        Index::putMapping('resources', function (Mapping $mapping) {
            // Would have been nice to have the `array` type here since it's more a list than an object but the library
            // seems to not support it at the moment
            $mapping->nested('ontology_terms');
        });
    }

    /**
     * Reverse the migration.
     */
    public function down(): void {}
}
