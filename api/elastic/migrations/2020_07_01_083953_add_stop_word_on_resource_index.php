<?php

declare(strict_types=1);

use Elastic\Adapter\Indices\Settings;
use Elastic\Migrations\Facades\Index;
use Elastic\Migrations\MigrationInterface;

final class AddStopWordOnResourceIndex implements MigrationInterface
{
    /**
     * Run the migration.
     */
    public function up(): void
    {
        Index::pushSettings('resources', static function (Settings $settings) {
            $settings->analysis([
                'analyzer' => [
                    'my_stop_word_analyzer' => [
                        'tokenizer' => 'whitespace',
                        'filter' => ['stop', 'lowercase'],
                    ],
                ],
            ]);
        });
    }

    /**
     * Reverse the migration.
     */
    public function down(): void
    {
        //
    }
}
