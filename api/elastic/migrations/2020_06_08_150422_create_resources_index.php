<?php

declare(strict_types=1);

use Elastic\Adapter\Indices\Mapping;
use Elastic\Adapter\Indices\Settings;
use Elastic\Migrations\Facades\Index;
use Elastic\Migrations\MigrationInterface;

final class CreateResourcesIndex implements MigrationInterface
{
    public function up(): void
    {
        Index::create('resources', function (Mapping $mapping, Settings $settings) {
            $mapping->text('title');
            // Would have been nice to have the `array` type here since it's more a list than an object but the library
            // seems to not support it at the moment
            $mapping->nested('categories');
            $mapping->text('description');
        });
    }

    public function down(): void
    {
        Index::dropIfExists('resources');
    }
}
