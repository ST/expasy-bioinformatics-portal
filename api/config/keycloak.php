<?php

return [
    'ssl_enabled' => env('KEYCLOAK_SSL_ENABLED', true),
];
