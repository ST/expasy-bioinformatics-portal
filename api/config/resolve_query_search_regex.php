<?php

use App\Enums\ExpasyQueryType;

return [
    ExpasyQueryType::UNIPROTID => ['/^([A-Z0-9]{1,5}_[A-Z0-9]{3,5}|([OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]([A-Z][A-Z0-9]{2}[0-9]){1,2})_[A-Z0-9]{3,5})$/'],
    ExpasyQueryType::UNIPROTAC => ['/^[OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z]([0-9][A-Z][A-Z0-9]{2}){1,2}[0-9]$/'],
    ExpasyQueryType::UNIPARC => ['/^UPI[A-Z0-9]{10}/'],
    ExpasyQueryType::PDBID => ['/^[1-9][A-Za-z0-9]{3}$/'],
    ExpasyQueryType::REFSEQ => ['/^[NXAY]P_\d{6}(\d{3})?(.\d+)?$/', '/^ZP_\d{8}(.\d+)?$/', '/^[NX][MR]_\d{6}(\d{3})?(.\d+)?$/', '/^N[CGTS]_\d{6}(.\d+)?$/', '/^AC_\d{6}(.\d+)?$/', '/^NW_\d{6}(\d{3})?(.\d+)?$/', '/^NZ_[A-Z]{4}\d{8}(.\d+)?$/', '/^WP_[0-9]{9}$/'],
    ExpasyQueryType::ENSEMBLID => ['/^ENS([A-Z]{3})?[GTEPR][0-9]{11}$/'],
    ExpasyQueryType::GBA => ['/^([A-Z]\d{5})|([A-Z]{2}\d{6,})/', '/^[A-Z]{3}\d{5,}(\.\d+)?/'],
    ExpasyQueryType::EC => ['/^[1-6]\.[\d\-]+\.[\d\-]+\.[\d\-]+$/'],
];
