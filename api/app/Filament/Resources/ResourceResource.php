<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ResourceResource\Pages;
use App\Forms\Components\NestedTreeAttachMany;
use App\Models\OntologyTerm;
use App\Models\Resource as ResourceModel;
use Filament\Forms\Components\Tabs;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\IconColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;

class ResourceResource extends Resource
{
    protected static ?string $model = ResourceModel::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Tabs::make('Tabs')
                    ->tabs([
                        // Tabs\Tab::make('Content')
                        //     ->schema([
                        //         TextInput::make('title')
                        //             ->label('Title')
                        //             ->required(),

                        //     ]),
                        Tabs\Tab::make('Classification')
                            ->schema([
                                NestedTreeAttachMany::make('ontology_terms')
                                    ->label('Ontology terms')
                                    ->items(OntologyTerm::getDagTree())
                                    ->default([1, 2, 3])
                                    ->reactive()
                                    ->required(),
                                TextInput::make('title'),
                                //             ->label('Title')
                                //             ->required(),

                            ]),
                        Tabs\Tab::make('Technical Information')
                            ->schema([
                                // ...
                            ]),
                    ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->striped()
            ->defaultSort('id', 'desc')
            ->searchable(false)
            ->defaultPaginationPageOption(50)
            ->paginated([25, 50, 100])
            ->columns([
                TextColumn::make('id')
                    ->searchable()
                    ->sortable(),
                TextColumn::make('title')
                    ->searchable()
                    ->sortable(),
                TextColumn::make('sIBGroup.name')
                    ->label('SIB Group')
                    ->searchable()
                    ->sortable(),

                IconColumn::make('pinned')
                    ->label('Pinned on homepage?')
                    ->boolean(),
                IconColumn::make('activated')
                    ->boolean(),
                TextColumn::make('created_at')
                    ->searchable()
                    ->sortable(),
                TextColumn::make('updated_at')
                    ->searchable()
                    ->sortable(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListResources::route('/'),
            'create' => Pages\CreateResource::route('/create'),
            'edit' => Pages\EditResource::route('/{record}/edit'),
        ];
    }
}
