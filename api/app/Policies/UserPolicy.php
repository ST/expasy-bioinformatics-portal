<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function view(User $loggedUser, User $user)
    {
        return $loggedUser->isSuperAdmin();
    }

    public function viewAny(User $user)
    {
        return $user->isSuperAdmin();
    }
}
