<?php

namespace App\Policies;

use App\Models\Resource;
use App\Models\ResourceCategory;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ResourcePolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        return true;
    }

    public function update(User $user, Resource $resource)
    {
        return $this->canRUD($user, $resource);
    }

    public function view(User $user, Resource $resource)
    {
        return $this->canRUD($user, $resource);
    }

    public function delete(User $user, Resource $resource)
    {
        return $this->canRUD($user, $resource);
    }

    public function viewAny(User $user)
    {
        return true;
    }

    public function detachResourceCategory(User $user, Resource $resource, ResourceCategory $category)
    {
        // This is for removing the bin next to each resource categories.
        return false;
    }

    public function attachResourceCategory(User $user, Resource $resource)
    {
        // This remove the edit button next to each resource categories.
        return $resource->id === null;
    }

    public function attachAnyResourceCategory(User $user, Resource $resource)
    {
        // Allow us to hide the "Attach Category" button. We only want to edit this relationship while updating
        // the resource.
        return false;
    }

    private function canRUD(User $user, ?Resource $resource = null): bool
    {
        $condition = $user->isSuperAdmin();

        if ($resource !== null) {
            $condition = $condition || $user->sib_groups->pluck('id')->contains(optional($resource->sib_group)->id);
        }

        return $condition;
    }
}
