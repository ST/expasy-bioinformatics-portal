<?php

namespace App\Libraries;

use App\Models\Resource;
use Illuminate\Support\Arr;
use MeiliSearch\Client;
use Meilisearch\Search\SearchResult;

class SearchMeilisearch
{
    private const MAX_RESULT_PER_PAGE = 1000;

    public function find(string $query, ?string $type): array
    {
        $results = $this->getResults($query, $type);
        $resultHits = $results->getHits();

        if (count($resultHits) === 0) {
            $exactMatch = false;
            $results = $this->getResults($query, 'fuzzy');
            $resultHits = $results->getHits();
        } else {
            $exactMatch = true;
        }

        $ids = array_map(function ($result) {
            return $result['id'];
        }, $resultHits);

        return [
            Resource::basicQuery()->whereIn('id', $ids)->get(),
            $exactMatch ? [] : $this->extractEmWords($resultHits),
            $exactMatch,
            $results->getRaw(),
        ];
    }

    protected function getResults(string $query, ?string $type): SearchResult
    {
        $query = str($query)->ascii();

        $client = new Client(config('scout.meilisearch.host'), config('scout.meilisearch.key'));
        $index = $client->getIndex('resources');

        // By default  try the exact match
        if (substr($query, 0, 1) === '"') {
            $query = substr($query, 1);
        }
        if (substr($query, -1) === '"') {
            $query = substr($query, 0, -1);
        }
        if ($type === 'fuzzy') {
            $query = $query;
        } else {
            $query = '"'.$query.'"';
        }
        $searchParams = $this->getSearchParams($query);

        return $index->search($query, $searchParams);
    }

    protected function getApproximateTerms(SearchResult $results): array
    {

        return $this->extractEmWords($results->getHits());
    }

    private function getSearchParams(string $query): array
    {
        $searchParams = [
            'limit' => 150,
            'attributesToHighlight' => [
                'title',
                'description',
                'short_description',
                'group_info',
            ],
        ];

        return $searchParams;
    }

    public function extractEmWords(array $hits)
    {
        $emWords = [];
        foreach ($hits as $data) {
            // dd($data['_formatted']);
            foreach ($data['_formatted'] as $key => $value) {

                if (! is_string($value)) {
                    continue;
                }
                preg_match_all('/<em>(.*?)<\/em>/', $value, $matches);

                // Flatten the array of matches and remove empty values
                $matches = array_filter(Arr::flatten($matches));
                // Remove <em> and </em> from each match
                $matches = array_map(function ($match) {
                    return str_replace(['<em>', '</em>'], '', $match);
                }, $matches);
                // Merge the matches into the result array
                $emWords = array_merge($emWords, $matches);
            }
        }

        // Transform to lowercase and remove duplicate words
        $emWords = array_values(array_unique(array_map('strtolower', $emWords)));

        return $emWords;
    }
}
