<?php

namespace App\Libraries;

use App\Models\SibGroup;
use App\Models\User;
use Exception;
use RuntimeException;

class KeycloakAuthUserService
{
    public const KEYCLOAK_ADMIN_ROLE = 'expasy-admin';

    public const KEYCLOAK_WRITER_ROLE = 'expasy-writer';

    public const KEYCLOAK_ROLES = [
        self::KEYCLOAK_ADMIN_ROLE,
        self::KEYCLOAK_WRITER_ROLE,
    ];

    public static function findOrCreateUserModels(array $user): User
    {
        $expasyRoles = collect($user['expasyrole']);
        if (! $expasyRoles->contains(self::KEYCLOAK_WRITER_ROLE) && ! $expasyRoles->contains(self::KEYCLOAK_ADMIN_ROLE)) {
            throw new RuntimeException('Invalid Keycloak role');
        }

        try {
            $groupIds = self::createGroupIfNotExists($user);
            $authUser = self::findOrCreateUser($user);
            $authUser->sib_groups()->sync($groupIds);
        } catch (Exception $e) {
            throw new RuntimeException('Error while trying to log you. Please try again.');
        }

        return $authUser;
    }

    private static function createGroupIfNotExists($user): array
    {
        return array_map(static function ($group) {
            $ldapId = ltrim($group, '/');

            return SibGroup::firstOrCreate(['ldap_identifier' => $ldapId], ['name' => $ldapId])->id;
        }, $user['sibgroup']);
    }

    private static function findOrCreateUser($user): User
    {
        $keycloakUserHasSuperAdminRole = collect($user['expasyrole'])->contains(self::KEYCLOAK_ADMIN_ROLE);

        return User::updateOrCreate(
            ['keycloak_id' => $user['sub']],
            ['super_admin' => $keycloakUserHasSuperAdminRole, 'name' => $user['name']]
        );
    }
}
