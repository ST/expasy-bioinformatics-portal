<?php

namespace App\Libraries;

use App\Enums\ExpasyQueryType;
use DB;
use GuzzleHttp\Client;
use Illuminate\Support\Str;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;
use RuntimeException;

class DBResourceSearchQuery
{
    public function fetchData(string $metaSearchUrl, string $query, ?ExpasyQueryType $resourceQueryType): object
    {
        $metaSearchUrl = $this->buildUrl($metaSearchUrl, $query, $resourceQueryType);
        $response = (new Client([
            'timeout' => env('GUZZLE_TIMEOUT', 20),
        ]))->get($metaSearchUrl);

        $this->validateResponse($response);

        $data = $this->getObjectFromRequestBody($response->getHeader('Content-Type')[0], $response->getBody());

        if ((int) $data->count === -1) {
            // We don't want an exception
            $data->count = 0;
        }

        return $data;
    }

    protected function buildUrl(string $metaSearchUrl, string $query, ?ExpasyQueryType $resourceQueryType): string
    {
        $queryType = $this->resolveQueryType($query);

        $metaSearchUrl .= sprintf(
            '%squery=%s',
            parse_url($metaSearchUrl, PHP_URL_QUERY) !== null ? '&' : '?',
            $query
        );

        $type = 'text';
        if ($queryType !== null && $resourceQueryType !== null && $resourceQueryType->hasFlag($queryType->value)) {
            $type = $queryType->description;
        }

        $metaSearchUrl .= "&type=$type";

        return $metaSearchUrl;
    }

    public function resolveQueryType(string $query): ?ExpasyQueryType
    {
        foreach (config('resolve_query_search_regex') as $queryType => $regexes) {
            foreach ($regexes as $regex) {
                preg_match($regex, $query, $matches);

                if ($matches !== false && count($matches) > 0 && $matches[0] === $query) {
                    return ExpasyQueryType::fromValue($queryType);
                }
            }
        }

        return null;
    }

    protected function getObjectFromRequestBody(string $contentType, StreamInterface $body): object
    {
        // SwissDock returns Xml content with Html header
        if (Str::contains($contentType, ['xml', 'html'])) {
            return simplexml_load_string($body);
        }

        return json_decode($body);
    }

    protected function validateResponse(ResponseInterface $curlRequest): void
    {
        if ($curlRequest === null || $curlRequest->getStatusCode() !== 200) {
            DB::disconnect();
            throw new RuntimeException("Error [{$curlRequest->getStatusCode()}] while fetching url");
        }

        $contentType = $curlRequest->getHeader('Content-Type')[0];
        if (! Str::contains($contentType, ['xml', 'html', 'json'])) {
            throw new RuntimeException("Invalid content type [$contentType]");
        }
    }
}
