<?php

namespace App\Providers;

use App\Exceptions\Handler;
use App\Http\Controllers\Nova\LoginController;
use App\Models\ResourceCategory;
use App\Models\ResourceLicenseType;
use App\Nova\Dashboards\Main;
use App\Observers\ResourceCategoryObserver;
use App\Observers\ResourceLicenseTypeObserver;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Schema;
use Laravel\Nova\Exceptions\NovaExceptionHandler;
use Laravel\Nova\Nova;
use Laravel\Nova\NovaApplicationServiceProvider;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        /**
         * Bypassing Keycloak in local environment
         */
        if (app()->environment('local') && Schema::hasTable('users')) {
            auth()->loginUsingId(1);
        }

        $this->app->alias(
            LoginController::class,
            \Laravel\Nova\Http\Controllers\LoginController::class
        );

        Nova::serving(static function () {
            ResourceCategory::observe(ResourceCategoryObserver::class);
            ResourceLicenseType::observe(ResourceLicenseTypeObserver::class);
        });

        Nova::style('admin', public_path('css/admin.css'));
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
            ->withAuthenticationRoutes()
            ->withPasswordResetRoutes()
            ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewNova', static function ($user) {
            return true;
        });
    }

    /**
     * Get the cards that should be displayed on the default Nova dashboard.
     *
     * @return array
     */
    protected function cards()
    {
        return [];
    }

    /**
     * Get the extra dashboards that should be displayed on the Nova dashboard.
     *
     * @return array
     */
    protected function dashboards()
    {
        return [
            new Main,
        ];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
        return [];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function registerExceptionHandler(): void
    {
        $this->app->bind(NovaExceptionHandler::class, Handler::class);
    }
}
