<?php

namespace App\Console\Commands;

use App\Models\OntologyTerm;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;
use RuntimeException;
use Telkins\Dag\Models\DagEdge;

class RefreshEdamOntologyTerms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ontology:refresh-terms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh Ontology terms with data from http://edamontology.org';

    protected string $edamUrl;

    protected string $edamCsvFileName;

    protected string $localCsvFileName;

    protected string $classIdRegex;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->edamUrl = 'http://edamontology.org';
        $this->edamCsvFileName = 'EDAM.csv';
        $this->localCsvFileName = 'edam-ontology.csv';
        $this->classIdRegex = '/^(http:\/\/edamontology.org\/)([a-z]+)_([0-9]+)$/';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->fetchOntologyTerms();
            $this->refreshOntologyTerms();
        } catch (Exception $e) {
            $this->error($e->getMessage());
        }
    }

    protected function fetchOntologyTerms()
    {
        $storage = Storage::disk('local');

        $this->output->write('Fetching ontology terms..');

        if (! $file = fopen("{$this->edamUrl}/{$this->edamCsvFileName}", 'rb')) {
            throw new RuntimeException("Error while trying to open file on {$this->edamUrl}/{$this->edamCsvFileName}");
        }

        $storage->put($this->localCsvFileName, $file);

        $this->output->writeln(' done');
    }

    protected function refreshOntologyTerms()
    {
        $csv = Reader::createFromPath(storage_path("app/{$this->localCsvFileName}"));
        $csv->setHeaderOffset(0);

        $allIds = [];

        DB::transaction(function () use ($csv, &$allIds) {
            collect($csv)
                ->filter(function ($row) {
                    return $row['Obsolete'] === 'FALSE' && preg_match($this->classIdRegex, $row['Class ID']);
                })
                ->each(function ($row) use (&$allIds) {
                    preg_match($this->classIdRegex, $row['Class ID'], $matches);

                    $ontologyTerm = OntologyTerm::updateOrCreate([
                        'edam_id' => $matches[3],
                        'type' => OntologyTerm::EDAM_TYPES[$matches[2]],
                    ], [
                        'name' => $row['Preferred Label'],
                    ]);
                    $allIds[] = $ontologyTerm->id;

                    $parentIds = [];
                    collect(explode('|', $row['Parents']))->map(function ($parentClassId) use (&$parentIds, &$allIds) {
                        preg_match($this->classIdRegex, $parentClassId, $matches);

                        if (count($matches) === 0) {
                            return;
                        }

                        $parent = OntologyTerm::firstOrCreate([
                            'edam_id' => $matches[3],
                            'type' => OntologyTerm::EDAM_TYPES[$matches[2]],
                        ]);
                        $parentIds[] = $parent->id;
                        $allIds[] = $parent->id;
                    })->toArray();
                    $ontologyTerm->addParents($parentIds);

                    $this->info("{$row['Class ID']} imported");
                });

            OntologyTerm::whereNotIn('id', $allIds)->delete();
            DagEdge::whereNotIn('start_vertex', $allIds)
                ->orWhereNotIn('end_vertex', $allIds)
                ->where('source', OntologyTerm::DAG_SOURCE)
                ->delete();
        });

        Cache::forget(OntologyTerm::TREE_CACHE_KEY);
        OntologyTerm::getDagTree();
    }
}
