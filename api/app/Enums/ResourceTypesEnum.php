<?php

namespace App\Enums;

use BenSampo\Enum\FlaggedEnum;

/**
 * @method static static DB()
 * @method static static TOOL()
 */
final class ResourceTypesEnum extends FlaggedEnum
{
    const DB = 1 << 0;

    const TOOL = 1 << 1;
}
