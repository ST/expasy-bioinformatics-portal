<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static TOPIC()
 * @method static static OPERATION()
 * @method static static DATA()
 * @method static static FORMAT()
 */
final class OntologyTypesEnum extends Enum
{
    const TOPIC = 0;

    const OPERATION = 1;

    const DATA = 2;

    const FORMAT = 3;
}
