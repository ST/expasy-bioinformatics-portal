<?php

namespace App\Enums;

use BenSampo\Enum\FlaggedEnum;

/**
 * @method static static UNIPROTID()
 * @method static static UNIPROTAC()
 * @method static static UNIPARC()
 * @method static static PDBID()
 * @method static static REFSEQ()
 * @method static static ENSEMBLID()
 * @method static static GBA()
 * @method static static EC()
 */
final class ExpasyQueryType extends FlaggedEnum
{
    const UNIPROTID = 1 << 0;

    const UNIPROTAC = 1 << 1;

    const UNIPARC = 1 << 2;

    const PDBID = 1 << 3;

    const REFSEQ = 1 << 4;

    const ENSEMBLID = 1 << 5;

    const GBA = 1 << 6;

    const EC = 1 << 7;

    public static function getDescription($value): string
    {
        switch ($value) {
            case self::UNIPROTAC:
                return 'UniProtAC';
            case self::UNIPROTID:
                return 'UniProtID';
            case self::UNIPARC:
                return 'UniParc';
            case self::PDBID:
                return 'PDBID';
            case self::REFSEQ:
                return 'REFSEQ';
            case self::ENSEMBLID:
                return 'EnsemblID';
            case self::GBA:
                return 'GBA';
            case self::EC:
                return 'EC';
            default:
                return parent::getDescription($value);
        }
    }
}
