<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\ResourceLicenseType
 *
 * @property int $id
 * @property string $title
 * @property string $link_label
 * @property string $link_url
 * @property-read \Illuminate\Support\Carbon $created_at
 * @property-read \Illuminate\Support\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Resource[] $resources
 * @property-read int|null $resources_count
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ResourceLicenseType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ResourceLicenseType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ResourceLicenseType query()
 * @method static \Illuminate\Database\Eloquent\Builder|ResourceLicenseType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResourceLicenseType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResourceLicenseType whereLinkLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResourceLicenseType whereLinkUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResourceLicenseType whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResourceLicenseType whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class ResourceLicenseType extends Model
{
    public const LICENSES = [
        'CC0',
        'CC-BY',
        'CC-BY SA',
        'CC-BY ND',
        'CC-BY NC',
        'CC-BY BY NC SA',
        'CC-BY BY NC ND',
        'GPL',
        'Apache',
        'MIT',
        'Restricted use',
        'Undefined',
    ];

    public function resources(): HasMany
    {
        return $this->hasMany(Resource::class);
    }
}
