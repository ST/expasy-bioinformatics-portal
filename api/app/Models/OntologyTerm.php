<?php

namespace App\Models;

use App\Enums\OntologyTypesEnum;
use App\Models\Traits\ExpasyDag;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\OntologyTerm
 *
 * @property-read int $id
 * @property string|null $name
 * @property int $edam_id
 * @property \App\Enums\OntologyTypesEnum $type
 * @property-read \App\Collections\DagCollection|OntologyTerm[] $ancestors
 * @property-read int|null $ancestors_count
 * @property-read \App\Collections\DagCollection|OntologyTerm[] $children
 * @property-read int|null $children_count
 * @property-read \App\Collections\DagCollection|OntologyTerm[] $decendants
 * @property-read int|null $decendants_count
 * @property-read \App\Collections\DagCollection|OntologyTerm[] $parents
 * @property-read int|null $parents_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Resource[] $resources
 * @property-read int|null $resources_count
 *
 * @method static \App\Collections\DagCollection|static[] all($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|OntologyTerm dagAncestorsOf($modelIds, string $source, ?int $maxHops = null)
 * @method static \Illuminate\Database\Eloquent\Builder|OntologyTerm dagDescendantsOf($modelIds, string $source, ?int $maxHops = null)
 * @method static \Illuminate\Database\Eloquent\Builder|OntologyTerm dagRelationsOf($modelIds, string $source, ?int $maxHops = null)
 * @method static \App\Collections\DagCollection|static[] get($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|OntologyTerm newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OntologyTerm newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OntologyTerm query()
 * @method static \Illuminate\Database\Eloquent\Builder|OntologyTerm whereEdamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OntologyTerm whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OntologyTerm whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OntologyTerm whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OntologyTerm withAncestors()
 * @method static \Illuminate\Database\Eloquent\Builder|OntologyTerm withAncestorsIds(?int $hops = null)
 * @method static \Illuminate\Database\Eloquent\Builder|OntologyTerm withParentsIds()
 *
 * @mixin \Eloquent
 */
class OntologyTerm extends Model
{
    use ExpasyDag;

    // There can be multiple Dags, they are differentiated by a source name
    const DAG_SOURCE = 'OntologyTerm';

    const TREE_CACHE_KEY = 'ontology_terms_tree_cache';

    const EDAM_TYPES = [
        'topic' => OntologyTypesEnum::TOPIC,
        'operation' => OntologyTypesEnum::OPERATION,
        'data' => OntologyTypesEnum::DATA,
        'format' => OntologyTypesEnum::FORMAT,
    ];

    /**
     * Existing casts are processed before $enumCasts which can be useful if you're
     * taking input from forms and your enum values are integers.
     */
    protected $casts = [
        'type' => 'int',
        'type' => OntologyTypesEnum::class,
    ];

    protected $fillable = [
        'name', 'edam_id', 'type',
    ];

    protected $touches = ['resources'];

    public $timestamps = false;

    public function resources(): BelongsToMany
    {
        return $this->belongsToMany(Resource::class);
    }
}
