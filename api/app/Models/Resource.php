<?php

namespace App\Models;

use App\Enums\ExpasyQueryType;
use App\Enums\ResourceTypesEnum;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Laravel\Scout\Searchable;

/**
 * App\Models\Resource
 *
 * @property-read int $id
 * @property string $title
 * @property string $slug
 * @property \App\Enums\ResourceTypesEnum $type
 * @property string $url
 * @property string|null $meta_search_url
 * @property string $description
 * @property string $short_description
 * @property string|null $media_url
 * @property string $group_info
 * @property bool $activated
 * @property bool $pinned
 * @property \App\Enums\ExpasyQueryType $query_type
 * @property-read \Illuminate\Support\Carbon $created_at
 * @property-read \Illuminate\Support\Carbon $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder activated()
 * @method static \Illuminate\Database\Eloquent\Builder basicQuery()
 *
 * @property int|null $resource_license_type_id
 * @property int|null $sib_group_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ResourceCategory[] $categories
 * @property-read int|null $categories_count
 * @property-read array $query_types
 * @property-read array $types
 * @property-read \App\Collections\DagCollection|\App\Models\OntologyTerm[] $ontology_terms
 * @property-read int|null $ontology_terms_count
 * @property-read \App\Models\ResourceLicenseType|null $resource_license_type
 * @property-read \App\Models\SibGroup|null $sib_group
 *
 * @method static Builder|Resource newModelQuery()
 * @method static Builder|Resource newQuery()
 * @method static Builder|Resource query()
 * @method static Builder|Resource whereActivated($value)
 * @method static Builder|Resource whereCreatedAt($value)
 * @method static Builder|Resource whereDescription($value)
 * @method static Builder|Resource whereGroupInfo($value)
 * @method static Builder|Resource whereId($value)
 * @method static Builder|Resource whereMediaUrl($value)
 * @method static Builder|Resource whereMetaSearchUrl($value)
 * @method static Builder|Resource wherePinned($value)
 * @method static Builder|Resource whereQueryType($value)
 * @method static Builder|Resource whereResourceLicenseTypeId($value)
 * @method static Builder|Resource whereShortDescription($value)
 * @method static Builder|Resource whereSibGroupId($value)
 * @method static Builder|Resource whereSlug($value)
 * @method static Builder|Resource whereTitle($value)
 * @method static Builder|Resource whereType($value)
 * @method static Builder|Resource whereUpdatedAt($value)
 * @method static Builder|Resource whereUrl($value)
 *
 * @property-read \App\Models\SibGroup|null $sIBGroup
 *
 * @mixin \Eloquent
 */
class Resource extends Model
{
    use Searchable;

    // Resource with meta search (ids are hardcoded in the frontend part)
    const UNIPROTKB = 1;

    const MYHITS = 2;

    const SELECTOME = 3;

    const PROSITE = 4;

    const SWISSMODELREPO = 5;

    const SWISSDOCK = 6;

    const ENZYME = 7;

    const HAMAP = 8;

    const SWISSVAR = 9;

    const VIRALZONE = 10;

    const SWISS2PDAGE = 11;

    const BGEE = 12;

    const NEXTPROT = 13;

    const STRINGDB = 14;

    const ORTHODB = 15;

    const MIRORTHO = 16;

    const OMA = 17;

    const SWISSLIPIDS = 18;

    const MEANETX = 19;

    const VENOMZONE = 20;

    const CELLOSAURUS = 21;

    public $enumCasts = [
    ];

    /**
     * Existing casts are processed before $enumCasts which can be useful if you're
     * taking input from forms and your enum values are integers.
     */
    protected $casts = [
        'type' => 'int',
        'query_type' => 'int',
        'pinned' => 'boolean',
        'type' => ResourceTypesEnum::class,
        'query_type' => ExpasyQueryType::class,
    ];

    // temporary fix because middlewaer somewhere search for this instead of sib_group
    public function sIBGroup(): BelongsTo
    {
        return $this->sib_group();
    }

    public function sib_group(): BelongsTo
    {
        return $this->belongsTo(SibGroup::class);
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(ResourceCategory::class);
    }

    public function resource_license_type(): BelongsTo
    {
        return $this->belongsTo(ResourceLicenseType::class);
    }

    public function ontology_terms(): BelongsToMany
    {
        return $this->belongsToMany(OntologyTerm::class);
    }

    public function scopeActivated(Builder $query): Builder
    {
        return $query->where('activated', '=', true);
    }

    public function getTypesAttribute(): array
    {
        $type = ResourceTypesEnum::fromValue($this->type);

        return $type->getFlags();
    }

    public function getQueryTypesAttribute(): array
    {
        $queryType = ExpasyQueryType::fromValue($this->query_type);

        return $queryType->getFlags();
    }

    public function toSearchableArray(): array
    {
        $firstLevelIds = OntologyTerm::whereIn('name', array_keys(OntologyTerm::EDAM_TYPES))
            ->get()
            ->pluck('id')
            ->toArray();

        $ontologyTermsWithAncestorsIds = $this->ontology_terms()
            ->withAncestorsIds(0)
            ->get()
            ->map(static fn ($ontologyTerm) => array_merge([$ontologyTerm->id], explode(',', $ontologyTerm->ancestors_ids)))
            ->flatten()
            ->map(fn ($id) => (int) $id)
            ->filter(fn ($id) => ! in_array($id, $firstLevelIds))
            ->unique();

        return [
            // Really hacky here but unfortunately no time to get it work the ES way
            'title' => str_replace(['/', '-'], ' ', $this->title),
            'description' => $this->description,
            'short_description' => $this->short_description,
            'group_info' => strip_tags($this->group_info),
            // For the same reason as for the index, categories is a nested type because the library doesn't support
            // array type yet
            'categories' => $this->categories
                ->map(static fn ($category) => ['title' => str_replace('& ', '', $category->title)])
                ->toArray(),
            'ontology_terms' => OntologyTerm::whereIn('id', $ontologyTermsWithAncestorsIds)
                ->select('name')
                ->pluck('name')
                ->map(static fn ($name) => ['title' => $name])->toArray(),
        ];
    }

    public function scopeBasicQuery(Builder $query)
    {
        return $query->activated()
            ->with(['categories.parent', 'categories.children', 'resource_license_type']);
    }
}
