<?php

namespace App\Models\Traits;

use App\Collections\DagCollection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Cache;
use Kalnoy\Nestedset\NodeTrait;
use Telkins\Dag\Models\Traits\IsDagManaged;

/**
 * @method static Builder withAncestors()
 * @method static Builder withAncestorsIds()
 * @method static Builder withParentsIds()
 */
trait ExpasyDag
{
    use IsDagManaged;
    use NodeTrait;

    public static function getDagTree()
    {
        return Cache::rememberForever(self::TREE_CACHE_KEY, function () {
            return self::withParentsIds()->get()->toHierarchy();
        });
    }

    public function ancestors(): BelongsToMany
    {
        return $this->belongsToMany(self::class, 'dag_edges', 'start_vertex', 'end_vertex');
    }

    public function decendants(): BelongsToMany
    {
        return $this->belongsToMany(self::class, 'dag_edges', 'end_vertex', 'start_vertex');
    }

    public function parents(): BelongsToMany
    {
        return $this->ancestors()->where('hops', 0);
    }

    public function children(): BelongsToMany
    {
        return $this->decendants()->where('hops', 0);
    }

    public function scopeWithAncestors(Builder $query): Builder
    {
        return $query->withParentsIds()
            ->with([
                'ancestors' => function ($query) {
                    $query->withParentsIds();
                },
            ]);
    }

    public function scopeWithAncestorsIds(Builder $query, ?int $hops = null): Builder
    {
        return $query->addSelect([
            'ancestors_ids' => function ($query) use ($hops) {
                $query->selectRaw('GROUP_CONCAT(end_vertex)')
                    ->from('dag_edges')
                    ->whereColumn('start_vertex', $this->getQualifiedKeyName());

                if (! is_null($hops)) {
                    $query->where('hops', '<=', $hops);
                }
            },
        ]);
    }

    public function scopeWithParentsIds(Builder $query): Builder
    {
        return $query->addSelect([
            'parents_ids' => function ($query) {
                $query->selectRaw('GROUP_CONCAT(end_vertex)')
                    ->from('dag_edges')
                    ->whereColumn('start_vertex', $this->getQualifiedKeyName())
                    ->where('hops', 0);
            },
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function newCollection(array $models = [])
    {
        return new DagCollection($models);
    }

    /**
     * @param  int|array  $parents
     */
    public function addParents($parents)
    {
        $this->manageEdges($parents, 'up', 'create');
    }

    /**
     * @param  int|array  $parents
     */
    public function removeParents($parents)
    {
        $this->manageEdges($parents, 'up', 'delete');
    }

    /**
     * @param  int|array  $children
     */
    public function addChildren($children)
    {
        $this->manageEdges($children, 'down', 'create');
    }

    /**
     * @param  int|array  $children
     */
    public function removeChildren($children)
    {
        $this->manageEdges($children, 'down', 'delete');
    }

    /**
     * @param  int|array  $ids
     */
    protected function manageEdges($ids, string $direction = 'up', string $mode = 'create')
    {
        $ids = is_array($ids) ? $ids : [$ids];

        foreach ($ids as $id) {
            $this->manageEdge($id, $direction, $mode);
        }
    }

    protected function manageEdge(int $id, string $direction = 'up', string $mode = 'create')
    {
        $method = $mode === 'create' ? 'createEdge' : 'deleteEdge';

        if ($direction === 'up') {
            $child = $this->id;
            $parent = $id;
        } else {
            $child = $id;
            $parent = $this->id;
        }

        dag()->{$method}($child, $parent, self::DAG_SOURCE);
    }
}
