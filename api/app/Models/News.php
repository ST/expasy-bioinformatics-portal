<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\News
 *
 * @property int $id
 * @property string $image_path
 * @property string $image_url
 * @property bool $open_in_new_tab
 * @property string $body
 * @property bool $is_draft
 * @property \Illuminate\Support\Carbon $published_from
 * @property \Illuminate\Support\Carbon $published_to
 * @property-read \Illuminate\Support\Carbon $created_at
 * @property-read \Illuminate\Support\Carbon $updated_at
 *
 * @method static Builder published()
 * @method static Builder|News newModelQuery()
 * @method static Builder|News newQuery()
 * @method static Builder|News query()
 * @method static Builder|News whereBody($value)
 * @method static Builder|News whereCreatedAt($value)
 * @method static Builder|News whereId($value)
 * @method static Builder|News whereImagePath($value)
 * @method static Builder|News whereImageUrl($value)
 * @method static Builder|News whereIsDraft($value)
 * @method static Builder|News whereOpenInNewTab($value)
 * @method static Builder|News wherePublishedFrom($value)
 * @method static Builder|News wherePublishedTo($value)
 * @method static Builder|News whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class News extends Model
{
    protected $casts = [
        'id' => 'integer',
        'open_in_new_tab' => 'boolean',
        'is_draft' => 'boolean',
        'published_from' => 'datetime',
        'published_to' => 'datetime',
    ];

    public function scopePublished(Builder $query): Builder
    {
        $now = now();

        return $query->where('is_draft', false)
            ->whereDate('published_from', '<=', $now)
            ->where(function ($query) use ($now) {
                $query->whereDate('published_to', '>=', $now)
                    ->orWhereNull('published_to');
            });
    }
}
