<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\SibGroup
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @property string $ldap_identifier
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Resource[] $resources
 * @property-read int|null $resources_count
 *
 * @method static \Illuminate\Database\Eloquent\Builder|SibGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SibGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SibGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|SibGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SibGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SibGroup whereLdapIdentifier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SibGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SibGroup whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class SibGroup extends Model
{
    protected $fillable = ['name', 'ldap_identifier'];

    public function resources(): HasMany
    {
        return $this->hasMany(Resource::class);
    }
}
