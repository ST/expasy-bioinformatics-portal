<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * App\Models\ResourceCategory
 *
 * @property int $id
 * @property string $title
 * @property int $parent_id
 * @property string $icon
 * @property-read \Illuminate\Support\Carbon $created_at
 * @property-read \Illuminate\Support\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|ResourceCategory[] $children
 * @property-read int|null $children_count
 * @property-read ResourceCategory|null $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Resource[] $resources
 * @property-read int|null $resources_count
 *
 * @method static \Illuminate\Database\Eloquent\Builder|ResourceCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ResourceCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ResourceCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|ResourceCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResourceCategory whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResourceCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResourceCategory whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResourceCategory whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResourceCategory whereUpdatedAt($value)
 *
 * @mixin \Eloquent
 */
class ResourceCategory extends Model
{
    /**
     * Categories's parent icons are hardcoded in a seed and user cannot modify theme. But if a new parent category
     * is created, customer wants to be able to select trough a list of 3 generic icons.
     */
    public const EXTRA_ICONS = [
        'heart-desktop' => 'Heart desktop',
        'pill-laptop' => 'Pill laptop',
        'placeholder' => 'Placeholder',
    ];

    public function parent(): BelongsTo
    {
        return $this->belongsTo(self::class);
    }

    public function children(): HasMany
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function resources(): BelongsToMany
    {
        return $this->belongsToMany(Resource::class);
    }
}
