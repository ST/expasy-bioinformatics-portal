<?php

namespace App\Forms\Components;

use Filament\Forms\Components\Field;
use Illuminate\Cache\TCacheValue;
use Illuminate\Support\Collection;

class NestedTreeAttachMany extends Field
{
    protected string $view = 'forms.components.nested-tree-attach-many';

    protected array|Collection|TCacheValue $items = [];

    public function items(array|Collection|TCacheValue $items): static
    {
        $this->items = $items;

        return $this;
    }

    public function getItems(): array|Collection|TCacheValue
    {
        return $this->items;
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->default([]);

        $this->afterStateHydrated(static function (Field $component, $state): void {
            // dd($component->getName());
            $state = $component->getRecord()->{$component->getName()}->pluck('id')->toArray();

            // dd($state);

            $component->state($state);
        });
    }
}
