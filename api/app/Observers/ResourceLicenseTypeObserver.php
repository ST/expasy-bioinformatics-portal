<?php

namespace App\Observers;

use App\Exceptions\HttpException;
use App\Models\ResourceLicenseType;

class ResourceLicenseTypeObserver
{
    /**
     * @throws HttpException
     */
    public function deleting(ResourceLicenseType $licenseType)
    {
        if ($licenseType->resources->count() > 0) {
            throw new HttpException('This license cannot be deleted. It is linked to other resources');
        }
    }
}
