<?php

namespace App\Observers;

use App\Exceptions\HttpException;
use App\Models\ResourceCategory;

class ResourceCategoryObserver
{
    /**
     * @throws HttpException
     */
    public function deleting(ResourceCategory $category)
    {
        if ($category->resources->count() > 0) {
            throw new HttpException('This category cannot be deleted. It is linked to other resources');
        }

        if ($category->children->count() > 0) {
            throw new HttpException('This category cannot be deleted. It has children');
        }
    }
}
