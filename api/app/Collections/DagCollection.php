<?php

namespace App\Collections;

use Kalnoy\Nestedset\Collection;

class DagCollection extends Collection
{
    public function toHierarchy()
    {
        $dictionary = $this->getDictionary();
        $nestedKeys = [];

        foreach ($dictionary as $key => $item) {
            if ($item->relationLoaded('ancestors')) {
                $dictionary += $item->ancestors->getDictionary();
            }
        }

        foreach ($dictionary as $key => $item) {
            $item->setRelation('children', new DagCollection);
        }

        foreach ($dictionary as $key => $item) {
            $parentsIds = $item->parents_ids ? explode(',', $item->parents_ids) : [];

            foreach ($parentsIds as $parentId) {
                if (array_key_exists($parentId, $dictionary)) {
                    $dictionary[$parentId]->children[] = $item;
                    $nestedKeys[] = $item->getKey();
                }
            }
        }

        foreach ($nestedKeys as $key) {
            unset($dictionary[$key]);
        }

        return new Collection($dictionary);
    }
}
