<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class HttpException extends \Exception implements HttpExceptionInterface
{
    public function getStatusCode(): int
    {
        // Nova needs a 500 to show message in a toast :(
        // https://github.com/laravel/nova-issues/issues/981
        // https://laracasts.com/discuss/channels/nova/laravel-nova-throw-exception-in-observer?page=1#reply=611963
        return 500;
    }

    public function getHeaders(): array
    {
        return [];
    }
}
