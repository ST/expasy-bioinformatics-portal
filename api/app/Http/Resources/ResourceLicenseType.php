<?php

namespace App\Http\Resources;

class ResourceLicenseType extends JsonResource
{
    public function toArray($request)
    {
        /** @var \App\Models\ResourceLicenseType $resource */
        $resource = $this->resource;

        return [
            'id' => $resource->id,
            'title' => $resource->title,
            'link_url' => $resource->link_url,
            'link_label' => $resource->link_label,
            'created_at' => $resource->created_at,
            'updated_at' => $resource->updated_at,
        ];
    }
}
