<?php

namespace App\Http\Resources;

class OntologyTermSelectTree extends JsonResource
{
    public function toArray($request)
    {
        /** @var \App\Models\OntologyTerm $ontologyTerm */
        $ontologyTerm = $this->resource;

        $data = [
            'id' => $ontologyTerm->id,
            'name' => $ontologyTerm->name,
            'children' => OntologyTermSelectTree::collection($ontologyTerm->children),
        ];

        return $data;
    }
}
