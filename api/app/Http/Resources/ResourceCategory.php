<?php

namespace App\Http\Resources;

class ResourceCategory extends JsonResource
{
    public function toArray($request)
    {
        /** @var \App\Models\ResourceCategory $category */
        $category = $this->resource;
        $parent = $category->parent;

        return [
            'id' => $category->id,
            'title' => $category->title,
            'icon' => $category->parent_id ? $parent->icon : $category->icon,
            'children' => $category->children,
        ];
    }
}
