<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\Storage;

/**
 * @property-read \App\Models\News $resource
 */
class News extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'image' => $this->resource->image_path
                ? Storage::url($this->resource->image_path)
                : null,
            'image_url' => $this->resource->image_url,
            'open_in_new_tab' => $this->resource->open_in_new_tab,
            'body' => $this->resource->body,
            'published_from' => $this->resource->published_from,
            'published_to' => $this->resource->published_to,
            'created_at' => $this->resource->created_at,
            'updated_at' => $this->resource->updated_at,
        ];
    }
}
