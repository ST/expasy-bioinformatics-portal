<?php

namespace App\Http\Resources;

class OntologyTerm extends JsonResource
{
    public function toArray($request)
    {
        /** @var \App\Models\OntologyTerm $resource */
        $resource = $this->resource;

        return [
            'id' => $resource->id,
            'name' => $resource->name,
            'type' => $resource->type,
        ];
    }
}
