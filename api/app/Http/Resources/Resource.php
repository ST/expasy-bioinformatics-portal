<?php

namespace App\Http\Resources;

use App\Enums\OntologyTypesEnum;
use App\Models\Resource as ResourceModel;
use Illuminate\Database\Eloquent\Collection;

class Resource extends JsonResource
{
    protected static array $using = [
        'isFull' => false,
    ];

    public function toArray($request): array
    {
        /** @var ResourceModel $resource */
        $resource = $this->resource;
        $relatedResources = [];
        if (self::$using['isFull']) {
            $relatedResources = $this->getRelatedResources();
        }

        return [
            'id' => $resource->id,
            'title' => $resource->title,
            'slug' => $resource->slug,
            'pinned' => $resource->pinned,
            'types' => ResourceType::collection(collect($resource->types)),
            'url' => $resource->url,
            'documentation_url' => $resource->documentation_url,
            'tutorial_url' => $resource->tutorial_url,
            'short_description' => $resource->short_description,
            'license_type' => (new ResourceLicenseType($resource->resource_license_type)),
            'categories' => ResourceCategory::collection($resource->categories),
            'created_at' => $resource->created_at,
            'updated_at' => $resource->updated_at,

            $this->mergeWhen(self::$using['isFull'], [
                'description' => nl2br($resource->description),
                'media_url' => $resource->media_url,
                'group_info' => $resource->group_info,

                $this->mergeWhen($resource->relationLoaded('ontology_terms'), [
                    'ontology_terms' => OntologyTerm::collection($resource->ontology_terms),
                    'related_resources' => self::collection($relatedResources),
                ]),
            ]),
        ];
    }

    protected function getRelatedResources(): Collection|array
    {
        /** @var ResourceModel $resource */
        $resource = $this->resource;

        if (! $resource->relationLoaded('ontology_terms')) {
            return [];
        }

        $ontologyIds = $resource->ontology_terms->filter(function ($ontologyTerm) {
            /** @var \App\Models\OntologyTerm $ontologyTerm */
            return $ontologyTerm->type->value === OntologyTypesEnum::TOPIC;
        })->pluck('id');

        return ResourceModel::basicQuery()->where('id', '!=', $resource->id)
            ->whereHas('ontology_terms', function ($query) use ($ontologyIds) {
                $query->whereIn('ontology_terms.id', $ontologyIds);
            })->get();
    }
}
