<?php

namespace App\Http\Resources;

use App\Enums\ResourceTypesEnum;

class ResourceType extends JsonResource
{
    public function toArray($request): array
    {
        switch ($this->resource->value) {
            case ResourceTypesEnum::DB:
                return [
                    'id' => 'db',
                    'icon' => 'db',
                    'title' => 'Database',
                ];
                break;
            case ResourceTypesEnum::TOOL:
                return [
                    'id' => 'tool',
                    'icon' => 'tool',
                    'title' => 'Software tool',
                ];
                break;
        }

        return [];
    }
}
