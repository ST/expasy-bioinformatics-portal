<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource as JsonResourceParent;

class JsonResource extends JsonResourceParent
{
    protected static array $using = [];

    public static function using($using = [])
    {
        static::$using = array_merge(static::$using, $using);
    }
}
