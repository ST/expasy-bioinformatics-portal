<?php

namespace App\Http\Requests;

use App\Models\Resource;
use Illuminate\Foundation\Http\FormRequest;

class MetaSearchRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'query' => 'required',
            'meta_search_url' => 'required',
        ];
    }

    public function validationData()
    {
        $resource = $this->route('resource');
        if ($resource instanceof Resource) {
            // Needed in order to validate route parameters
            return array_merge($this->all(), [
                'meta_search_url' => $resource->meta_search_url,
            ]);
        }

        return $this->all();
    }
}
