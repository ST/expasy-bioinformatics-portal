<?php

namespace App\Http\Controllers;

use App\Libraries\KeycloakAuthUserService;
use App\Models\User;
use Exception;
use Filament\Facades\Filament;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    public function logout(Request $request)
    {

        Filament::auth()->logout();

        $request->session()->invalidate();

        $keycloakLogoutUrl = env('KEYCLOAK_BASE_URL')
            .'realms/'.env('KEYCLOAK_REALMS').'/protocol/openid-connect/logout?client_id='.env('KEYCLOAK_CLIENT_ID').'&post_logout_redirect_uri='.url('/');

        return redirect($keycloakLogoutUrl);

    }

    /**
     * Redirect the user to the Keycloak authentication page.
     */
    public function redirectToProvider()
    {
        config(['services.keycloak.redirect' => route('admin2.sso.redirect')]);

        return Socialite::driver('keycloak')->redirect();
    }

    /**
     * Obtain the user information from Keycloak.
     */
    public function handleProviderCallback(Request $request)
    {

        config(['services.keycloak.redirect' => route('admin2.sso.redirect')]);
        if ($request->has('error')) {
            return redirect(url('/login'))->with('error', $request->get('error_description'));
        }

        try {
            $user = Socialite::driver('keycloak')
                // ->stateless()
                // ->setHttpClient(new Client(['verify' => env('KEYCLOAK_SSL_ENABLED', true)]))
                ->user();
        } catch (Exception $e) {
            logger()->debug('Error', ['e' => $e->getMessage()]);

            return redirect(url('/login'))->with(['error' => $e]);
        }

        try {
            $authUser = KeycloakAuthUserService::findOrCreateUserModels($user->user);
        } catch (Exception $e) {
            dd($e->getMessage());

            return back()->with('error', $e->getMessage());
        }

        Auth::login($authUser, remember: session('login_remember', false));

        return redirect(url('/admin2'));
    }
}
