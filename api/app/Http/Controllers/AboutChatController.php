<?php

namespace App\Http\Controllers;

class AboutChatController extends Controller
{
    public function index()
    {
        return view('about-chat');
    }
}
