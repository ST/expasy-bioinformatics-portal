<?php

namespace App\Http\Controllers;

use App\Models\News;

class HomeController extends Controller
{
    public function index()
    {
        $viewData = [
            'lastNews' => News::published()->latest()->first(),
        ];

        return view('home', $viewData);
    }
}
