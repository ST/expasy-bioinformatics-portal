<?php

namespace App\Http\Controllers\Nova;

use App\Libraries\KeycloakAuthUserService;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends \Laravel\Nova\Http\Controllers\LoginController
{
    public function showLoginForm()
    {
        return redirect(route('sso.redirect'));

        return view('auth.login');
    }

    /**
     * Redirect the user to the Keycloak authentication page.
     */
    public function redirectToProvider()
    {
        return Socialite::driver('keycloak')->with(['scope' => 'openid'])->redirect();
    }

    /**
     * Obtain the user information from Keycloak.
     */
    public function handleProviderCallback(Request $request)
    {
        if ($request->has('error')) {
            return back()->with('error', $request->get('error_description'));
        }

        try {
            // At this point, Keycloak staging server has an unverified ssl cert so we need this option.
            // We might need to conditionally set this.
            $user = Socialite::driver('keycloak')
                ->stateless()
                ->setHttpClient(new Client(['verify' => config('keycloak.ssl_enabled')]))
                ->user();
        } catch (Exception $e) {
            return back()->with('error', 'Error while fetching user from Keycloak');
        }

        try {
            $authUser = KeycloakAuthUserService::findOrCreateUserModels($user->user);
        } catch (Exception $e) {
            return back()->with('error', $e->getMessage());
        }

        Auth::login($authUser, true);

        // https://github.com/laravel/nova-issues/issues/445
        // It should be `nova.index` but it doesn't work. Don't have time to investigate further.
        return redirect(config('nova.path'));
    }
}
