<?php

namespace App\Http\Controllers;

use App\Http\Resources\Resource as JsonResource;
use App\Libraries\SearchMeilisearch;

class SearchController extends Controller
{
    public function index($query, SearchMeilisearch $search)
    {

        config(['scout.driver' => 'meilisearch']);
        [$resources, $approximateTerms, $exactMatch, $results] = $search->find($query, request()->get('type'));

        $viewData = [
            'query' => $query,
            'result' => array_merge(
                [
                    'exact_match' => $exactMatch,
                    'resources' => JsonResource::collection($resources),
                    'approximate_terms' => $approximateTerms,
                    'scout' => config('scout.driver'),
                ],
                config('app.env') === 'local' ? ['results' => $results] : []
            ),
        ];

        return view('search', $viewData);
    }
}
