<?php

namespace App\Http\Controllers;

class ArchiveController extends Controller
{
    public function index($resourceSlug)
    {

        if (! file_exists(resource_path('views/archives/'.$resourceSlug.'.blade.php'))) {
            return view('errors.404');
        }

        $viewData = [
            'resourceSlug' => $resourceSlug,
        ];

        return view('archive', $viewData);
    }
}
