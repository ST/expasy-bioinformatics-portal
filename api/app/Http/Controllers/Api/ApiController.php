<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    /**
     * Render a standard api response in Json
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function renderJson($data)
    {
        return response()->json($data);
    }

    /**
     * Render a standard 404 resource not found
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function renderNotFound($message = 'Resource not found')
    {
        return $this->renderJsonError($message, 404);
    }

    /**
     * Render a standard api error in Json
     *
     * @param  int  $httpStatus
     * @return \Illuminate\Http\JsonResponse
     */
    public function renderJsonError($message, $httpStatus = 422)
    {
        $data = ['status' => $httpStatus, 'message' => $message];

        return response()->json(
            $data,
            $httpStatus
        );
    }
}
