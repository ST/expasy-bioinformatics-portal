<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\MetaSearchRequest;
use App\Http\Resources\Resource as JsonResource;
use App\Libraries\DBResourceSearchQuery;
use App\Libraries\SearchMeilisearch;
use App\Models\Resource;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SearchController extends ApiController
{
    public function search(Request $request): JsonResponse
    {
        return $this->searchMeilisearch($request, new SearchMeilisearch);

    }

    public function metaSearch(MetaSearchRequest $request, Resource $resource, DBResourceSearchQuery $DBResourceSearchQuery): JsonResponse
    {
        $validated = $request->validated();

        try {
            $data = $DBResourceSearchQuery->fetchData($validated['meta_search_url'], $validated['query'], $resource->query_type);
        } catch (Exception $e) {
            return $this->renderJsonError($e->getMessage());
        }

        return $this->renderJson([
            'count' => (int) $data->count,
            'url' => (string) $data->url,
        ]);
    }

    public function searchMeilisearch(Request $request, SearchMeilisearch $search): JsonResponse
    {
        config(['scout.driver' => 'meilisearch']);
        [$resources, $approximateTerms, $exactMatch, $results] = $search->find($request->get('query') ?: '', $request->get('type'));

        return $this->renderJson(
            array_merge(
                [
                    'exact_match' => $exactMatch,
                    'resources' => JsonResource::collection($resources),
                    'approximate_terms' => $approximateTerms,
                    'scout' => config('scout.driver'),
                ],
                config('app.env') === 'local' ? ['results' => $results] : []
            )
        );
    }
}
