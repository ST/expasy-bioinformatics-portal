<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Resources\News as JsonResource;
use App\Models\News;
use Illuminate\Http\JsonResponse;

class NewsController extends ApiController
{
    public function last(): JsonResponse
    {
        $resource = News::published()->latest()->first();

        $data = empty($resource) ? null : new JsonResource($resource);

        return $this->renderJson($data);
    }
}
