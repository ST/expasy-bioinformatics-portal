<?php

namespace App\Http\Controllers\Api\V1;

use App\Enums\ResourceTypesEnum;
use App\Http\Controllers\Api\ApiController;
use App\Http\Resources\ResourceCategory as ResourceCategoryJson;
use App\Http\Resources\ResourceType;
use App\Models\ResourceCategory;
use Illuminate\Http\JsonResponse;

class ResourceFilterController extends ApiController
{
    public function index(): JsonResponse
    {
        $categories = ResourceCategory::whereNull('parent_id')->get();
        /** @var \Illuminate\Support\Collection $types */
        $types = ResourceType::collection(ResourceTypesEnum::getInstances())->resource;

        return $this->renderJson([
            'categories' => ResourceCategoryJson::collection($categories),
            'types' => $types->forget('None')->values(),
        ]);
    }
}
