<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Resources\Resource as ResourceJson;
use App\Models\Resource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ResourceController extends ApiController
{
    /**
     * Return the list of resources
     */
    public function index(Request $request): JsonResponse
    {
        $fullResource = $request->has('schema') && $request->get('schema') === 'full';

        $cacheKeys = 'resources'
                    .($fullResource ? '_full' : '')
                    .($request->has('slug') ? '_'.$request->get('slug') : '');

        return Cache::remember(
            $cacheKeys,
            60 * 1, // 1 minute
            function () use ($request, $fullResource) {
                $resourcesQuery = Resource::basicQuery();

                if ($request->has('slug')) {
                    $resourcesQuery = $resourcesQuery->where('slug', $request->get('slug'));
                }

                if ($request->has('schema')) {
                    $fullResource = $request->get('schema') === 'full';
                }

                if ($fullResource) {
                    $resourcesQuery->with(['ontology_terms']);
                }

                ResourceJson::using(['isFull' => $fullResource]);

                return $this->renderJson(
                    ResourceJson::collection(
                        $resourcesQuery->get()
                    )
                );
            }
        );
    }

    /**
     * Return the resource detail
     */
    public function show($id): JsonResponse
    {
        $resource = Resource::basicQuery()->with(['ontology_terms'])->find($id);

        if (! $resource) {
            return $this->renderNotFound();
        }

        ResourceJson::using(['isFull' => true]);

        return $this->renderJson(new ResourceJson($resource));
    }
}
