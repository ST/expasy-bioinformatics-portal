<?php

namespace App\Nova;

use App\Models\ResourceCategory as ResourceCategoryModel;
use App\Rules\ValidateParent;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class ResourceCategory extends Resource
{
    public static $model = ResourceCategoryModel::class;

    public static $group = 'Resource';

    public static $title = 'title';

    public static $search = [
        'title',
    ];

    public static function label(): string
    {
        return 'Categories';
    }

    public static function availableForNavigation(Request $request)
    {
        return $request->user()->isSuperAdmin();
    }

    public function fields(NovaRequest $request): array
    {
        $resourceId = $request->resourceId;

        return [
            ID::make()->sortable(),

            Text::make('Title')
                ->rules('max:255', Rule::unique('resource_categories', 'title')->ignore($resourceId))
                ->nullable(),

            Select::make('Parent', 'parent_id')
                ->options(static fn () => ResourceCategoryModel::whereNull('parent_id')
                    ->where('id', '<>', $resourceId)
                    ->get()
                    ->pluck('title', 'id')
                    ->toArray())->displayUsingLabels()
                ->rules(new ValidateParent($resourceId))
                ->nullable(),

            Select::make('Icon')
                ->options(static fn () => ResourceCategoryModel::EXTRA_ICONS)
                ->onlyOnForms()
                ->hideWhenUpdating()
                ->nullable(),

        ];
    }
}
