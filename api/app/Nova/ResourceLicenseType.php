<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class ResourceLicenseType extends Resource
{
    public static $model = \App\Models\ResourceLicenseType::class;

    public static $group = 'Resource';

    public static $title = 'link_label';

    public static $search = [
        'id',
    ];

    public static function label(): string
    {
        return 'License types';
    }

    public static function availableForNavigation(Request $request)
    {
        return $request->user()->isSuperAdmin();
    }

    public function fields(NovaRequest $request): array
    {
        return [
            ID::make()->sortable(),

            Text::make('Title')
                ->hideFromIndex(),

            Text::make('Link label', 'link_label')
                ->rules('required', 'max:50'),

            Text::make('Link url', 'link_url')
                ->help('E.g. https://creativecommons.org/licenses/by-sa/4.0/')
                ->rules('required', 'url'),
        ];
    }
}
