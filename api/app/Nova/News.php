<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Manogi\Tiptap\Tiptap;

class News extends Resource
{
    public static $model = \App\Models\News::class;

    // public static $title = 'body';

    public static $search = [
        'body',
    ];

    public function title()
    {
        return strip_tags($this->body);
    }

    public static function label(): string
    {
        return 'News';
    }

    public static function availableForNavigation(Request $request): bool
    {
        return $request->user()->isSuperAdmin();
    }

    public function fields(NovaRequest $request): array
    {
        return [
            ID::make()->sortable(),

            Tiptap::make('Body')
                ->buttons([
                    'bold',
                    'italic',
                    'link',
                ])
                ->alwaysShow()
                ->rules('required', function ($attribute, $value, $fail) {
                    $charCountMax = 270;

                    if (strlen(strip_tags($value)) > $charCountMax) {
                        $fail("The {$attribute} may not be greater than {$charCountMax} characters.");
                    }
                }, 'not_in:<p></p>'),

            Image::make('Image', 'image_path')
                ->disk('public')
                ->maxWidth(200)
                ->path('attachments')
                ->disableDownload()
                ->hideFromIndex(),

            Text::make('Image link', 'image_url')
                ->rules('nullable', 'url')
                ->sortable(),

            Boolean::make('Open image link in new tab?', 'open_in_new_tab'),

            Boolean::make('Is draft?', 'is_draft')
                ->sortable(),

            DateTime::make('Published from')
                ->default(now())
                ->rules('required')
                ->sortable(),

            DateTime::make('Published to')
                ->rules('nullable', 'date', 'after:published_from')
                ->sortable(),

            DateTime::make('Created at')
                ->onlyOnDetail(),

            DateTime::make('Updated at')
                ->onlyOnDetail(),
        ];
    }
}
