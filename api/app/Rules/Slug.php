<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class Slug implements ValidationRule
{
    /**
     * Run the validation rule.
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (preg_match('/^[a-z0-9]+(?:-[a-z0-9]+)*$/', $value)) {
            return;
        }

        $fail('Slug required');

    }
}
