<?php

namespace App\Rules;

use App\Models\ResourceCategory as ResourceCategoryModel;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class ValidateParent implements ValidationRule
{
    protected ?string $resourceId;

    public function __construct(?string $resourceId)
    {
        $this->resourceId = $resourceId;
    }

    /**
     * Run the validation rule.
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if ($this->resourceId === null || ResourceCategoryModel::find($this->resourceId)->parent_id !== null) {
            return;
        }

        if (ResourceCategoryModel::where('parent_id', '=', $this->resourceId)->count() === 0) {
            return;
        }

        $fail('This category cannot have a parent since it is a parent itself.');

    }
}
