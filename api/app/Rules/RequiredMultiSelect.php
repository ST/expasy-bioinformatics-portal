<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class RequiredMultiSelect implements ValidationRule
{
    /**
     * Run the validation rule.
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $value = json_decode($value, true, 512, JSON_THROW_ON_ERROR);

        if (is_array($value)) {
            $value = array_filter($value);

            if (! empty($value)) {
                return;
            }
        }

        if ($value !== null && $value !== '' && $value !== '[]') {
            return;
        }

        $fail('Please choose at least one value.');

    }
}
