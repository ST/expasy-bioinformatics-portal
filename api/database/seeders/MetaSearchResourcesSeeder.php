<?php

namespace Database\Seeders;

use App\Enums\ExpasyQueryType;
use App\Enums\ResourceTypesEnum;
use App\Models\Resource;
use App\Models\ResourceCategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MetaSearchResourcesSeeder extends Seeder
{
    public function run(): void
    {
        // Remove all resources
        DB::table('resources')->delete();

        self::addMetaSearchResources();
    }

    protected static function addMetaSearchResources()
    {
        $searchableResources = [
            Resource::BGEE => [
                'id' => Resource::BGEE,
                'title' => 'Bgee',
                'slug' => 'bgee',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => ExpasyQueryType::flags([
                    ExpasyQueryType::EC,
                    ExpasyQueryType::GBA,
                    ExpasyQueryType::ENSEMBLID,
                    ExpasyQueryType::REFSEQ,
                    ExpasyQueryType::PDBID,
                    ExpasyQueryType::UNIPARC,
                    ExpasyQueryType::UNIPROTID,
                    ExpasyQueryType::UNIPROTAC,
                ]),
                'url' => 'https://bgee.org/',
                'meta_search_url' => 'https://bgee.org/?page=search&action=expasy_result&display_type=xml',
                'description' => '',
                'short_description' => 'Gene expression pattern comparison',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::GENES_AND_GENOMES,
            ],
            Resource::MEANETX => [
                'id' => Resource::MEANETX,
                'title' => 'MetaNetX',
                'slug' => 'metanetx',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => ExpasyQueryType::flags([
                    ExpasyQueryType::EC,
                    ExpasyQueryType::UNIPROTID,
                ]),
                'url' => 'https://www.metanetx.org/',
                'meta_search_url' => 'https://www.metanetx.org/cgi-bin/mnxweb/expasy',
                'description' => '',
                'short_description' => 'Metabolic Network Repository & Analysis',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::SYSTEMS_BIOLOGY,
            ],
            Resource::CELLOSAURUS => [
                'id' => Resource::CELLOSAURUS,
                'title' => 'Cellosaurus',
                'slug' => 'Cellosaurus',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => ExpasyQueryType::flags([
                    ExpasyQueryType::UNIPROTAC,
                    ExpasyQueryType::UNIPROTID,
                ]),
                'url' => 'https://web.expasy.org/cellosaurus/',
                'meta_search_url' => 'https://web.expasy.org/cgi-bin/sib_geneva_search?resource=cellosaurus',
                'description' => '',
                'short_description' => 'Knowledge resource on cell line',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::STRUCTURAL_BIOLOGY,
            ],
            Resource::SWISSMODELREPO => [
                'id' => Resource::SWISSMODELREPO,
                'title' => 'SWISS-MODEL',
                'slug' => 'swiss-model',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => ExpasyQueryType::flags([
                    ExpasyQueryType::UNIPROTAC,
                    ExpasyQueryType::UNIPROTID,
                ]),
                'url' => 'https://swissmodel.expasy.org/repository/',
                'meta_search_url' => 'https://swissmodel.expasy.org/service/sm_expasy_search.cgi',
                'description' => '',
                'short_description' => 'Protein structure homology models',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::STRUCTURAL_BIOLOGY,

            ],
            Resource::OMA => [
                'id' => Resource::OMA,
                'title' => 'OMA',
                'slug' => 'oma',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => ExpasyQueryType::flags([
                    ExpasyQueryType::UNIPROTAC,
                    ExpasyQueryType::UNIPROTID,
                ]),
                'url' => 'http://omabrowser.org/',
                'meta_search_url' => 'http://www.omabrowser.org/cgi-bin/sib-search.pl',
                'description' => '',
                'short_description' => 'Orthology inference among complete genomes',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::EVOLUTION_AND_PHYLOGENY,
            ],
            Resource::ORTHODB => [
                'id' => Resource::ORTHODB,
                'title' => 'OrthoDB',
                'slug' => 'ortho-db',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => ExpasyQueryType::flags([
                    ExpasyQueryType::UNIPROTAC,
                    ExpasyQueryType::UNIPROTID,
                ]),
                'url' => 'http://www.orthodb.org/',
                'meta_search_url' => 'https://data.orthodb.org/current/expasysearch',
                'description' => '',
                'short_description' => 'Hierachical catalog of eukaryiotic orthologs',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::EVOLUTION_AND_PHYLOGENY,
            ],
            Resource::MIRORTHO => [
                'id' => Resource::MIRORTHO,
                'title' => 'miROrtho',
                'slug' => 'mirortho',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => ExpasyQueryType::flags([
                    ExpasyQueryType::PDBID,
                    ExpasyQueryType::UNIPROTAC,
                    ExpasyQueryType::UNIPROTID,
                ]),
                'url' => 'http://cegg.unige.ch/mirortho',
                'meta_search_url' => 'http://cegg.unige.ch/rest/mirortho',
                'description' => '',
                'short_description' => 'Catalogue of animal microRNA genes',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::EVOLUTION_AND_PHYLOGENY,
            ],
            Resource::SELECTOME => [
                'id' => Resource::SELECTOME,
                'title' => 'Selectome',
                'slug' => 'selectome',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => ExpasyQueryType::flags([
                    ExpasyQueryType::EC,
                    ExpasyQueryType::GBA,
                    ExpasyQueryType::ENSEMBLID,
                    ExpasyQueryType::REFSEQ,
                    ExpasyQueryType::PDBID,
                    ExpasyQueryType::UNIPROTID,
                    ExpasyQueryType::UNIPROTAC,
                ]),
                'url' => 'https://selectome.org/',
                'meta_search_url' => 'https://selectome.org/requests?out=xmle',
                'description' => '',
                'short_description' => 'Positive selection',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::EVOLUTION_AND_PHYLOGENY,
            ],
            Resource::NEXTPROT => [
                'id' => Resource::NEXTPROT,
                'title' => 'neXtProt',
                'slug' => 'nextprot',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => ExpasyQueryType::flags([
                    ExpasyQueryType::EC,
                    ExpasyQueryType::ENSEMBLID,
                    ExpasyQueryType::REFSEQ,
                    ExpasyQueryType::PDBID,
                    ExpasyQueryType::UNIPARC,
                    ExpasyQueryType::UNIPROTID,
                    ExpasyQueryType::UNIPROTAC,
                ]),
                'url' => 'https://www.nextprot.org/',
                'meta_search_url' => 'https://api.nextprot.org/expasy-search.xml?type=text',
                'description' => '',
                'short_description' => 'Human proteins',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::PROTEINS_AND_PROTEOMES,
            ],
            Resource::PROSITE => [
                'id' => Resource::PROSITE,
                'title' => 'PROSITE',
                'slug' => 'Protein domains and families',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => ExpasyQueryType::flags([
                    ExpasyQueryType::PDBID,
                    ExpasyQueryType::UNIPROTID,
                    ExpasyQueryType::UNIPROTAC,
                ]),
                'url' => 'https://prosite.expasy.org/',
                'meta_search_url' => 'https://web.expasy.org/cgi-bin/sib_geneva_search?resource=prosite',
                'description' => '',
                'short_description' => 'Protein-protein interactions',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::PROTEINS_AND_PROTEOMES,
            ],
            Resource::STRINGDB => [
                'id' => Resource::STRINGDB,
                'title' => 'STRING',
                'slug' => 'string',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => ExpasyQueryType::flags([
                    ExpasyQueryType::ENSEMBLID,
                    ExpasyQueryType::REFSEQ,
                    ExpasyQueryType::UNIPROTID,
                    ExpasyQueryType::UNIPROTAC,
                ]),
                'url' => 'https://string-db.org/',
                'meta_search_url' => 'https://string-db.org/api/xml/expasy',
                'description' => '',
                'short_description' => 'Protein-protein interactions',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::PROTEINS_AND_PROTEOMES,
            ],
            Resource::UNIPROTKB => [
                'id' => Resource::UNIPROTKB,
                'title' => 'UniProtKB',
                'slug' => 'uniprotkb-swiss-prot',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => ExpasyQueryType::flags([
                    ExpasyQueryType::EC,
                    ExpasyQueryType::GBA,
                    ExpasyQueryType::ENSEMBLID,
                    ExpasyQueryType::REFSEQ,
                    ExpasyQueryType::PDBID,
                    ExpasyQueryType::UNIPARC,
                    ExpasyQueryType::UNIPROTID,
                    ExpasyQueryType::UNIPROTAC,
                ]),
                'url' => 'https://www.uniprot.org/',
                'meta_search_url' => 'https://web.expasy.org/cgi-bin/sib_geneva_search?resource=uniprot',
                'description' => '',
                'short_description' => 'Protein sequence database',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::PROTEINS_AND_PROTEOMES,
            ],
            Resource::VIRALZONE => [
                'id' => Resource::VIRALZONE,
                'title' => 'ViralZone',
                'slug' => 'viralzone',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => null,
                'url' => 'https://viralzone.expasy.org/',
                'meta_search_url' => 'https://web.expasy.org/cgi-bin/sib_geneva_search?resource=viralzone',
                'description' => '',
                'short_description' => 'Portal to viral UniProtKB',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::PROTEINS_AND_PROTEOMES,
            ],
            Resource::ENZYME => [
                'id' => Resource::ENZYME,
                'title' => 'ENZYME',
                'slug' => 'enzyme',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => ExpasyQueryType::flags([
                    ExpasyQueryType::EC,
                    ExpasyQueryType::UNIPROTID,
                    ExpasyQueryType::UNIPROTAC,
                ]),
                'url' => 'https://enzyme.expasy.org/',
                'meta_search_url' => 'https://web.expasy.org/cgi-bin/sib_geneva_search?resource=enzyme',
                'description' => '',
                'short_description' => 'Enzyme nomenclature',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::PROTEINS_AND_PROTEOMES,
            ],
            Resource::HAMAP => [
                'id' => Resource::HAMAP,
                'title' => 'HAMAP',
                'slug' => 'hamap',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => ExpasyQueryType::flags([
                    ExpasyQueryType::EC,
                    ExpasyQueryType::UNIPROTID,
                    ExpasyQueryType::UNIPROTAC,
                ]),
                'url' => 'https://hamap.expasy.org/',
                'meta_search_url' => 'https://web.expasy.org/cgi-bin/sib_geneva_search?resource=hamap',
                'description' => '',
                'short_description' => 'UniProtKB family classification and annotation',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::PROTEINS_AND_PROTEOMES,
            ],
            Resource::MYHITS => [
                'id' => Resource::MYHITS,
                'title' => 'MyHits',
                'slug' => 'myhits',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => ExpasyQueryType::flags([
                    ExpasyQueryType::EC,
                    ExpasyQueryType::GBA,
                    ExpasyQueryType::ENSEMBLID,
                    ExpasyQueryType::REFSEQ,
                    ExpasyQueryType::PDBID,
                    ExpasyQueryType::UNIPARC,
                    ExpasyQueryType::UNIPROTID,
                    ExpasyQueryType::UNIPROTAC,
                ]),
                'url' => 'https://myhits.isb-sib.ch/',
                'meta_search_url' => 'https://myhits.isb-sib.ch/cgi-bin/text_search',
                'description' => '',
                'short_description' => 'Protein domains database and tools',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::PROTEINS_AND_PROTEOMES,
            ],
            Resource::SWISS2PDAGE => [
                'id' => Resource::SWISS2PDAGE,
                'title' => 'SWISS-2DPAGE',
                'slug' => 'swiss-2dpage',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => ExpasyQueryType::flags([
                    ExpasyQueryType::UNIPROTID,
                    ExpasyQueryType::UNIPROTAC,
                ]),
                'url' => 'https://world-2dpage.expasy.org/swiss-2dpage/',
                'meta_search_url' => 'https://web.expasy.org/cgi-bin/sib_geneva_search?resource=swiss2dpage',
                'description' => '',
                'short_description' => 'Proteins on 2-D and SDS PAGE maps',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::PROTEINS_AND_PROTEOMES,
            ],
            Resource::SWISSLIPIDS => [
                'id' => Resource::SWISSLIPIDS,
                'title' => 'SwissLipids',
                'slug' => 'swiss-lipids',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => ExpasyQueryType::flags([
                    ExpasyQueryType::UNIPROTAC,
                ]),
                'url' => 'http://www.swisslipids.org/#/',
                'meta_search_url' => 'http://www.swisslipids.org/php/expasy.php',
                'description' => '',
                'short_description' => 'Knowledge resource for lipid biology',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::PROTEINS_AND_PROTEOMES,
            ],
            Resource::SWISSVAR => [
                'id' => Resource::SWISSVAR,
                'title' => 'SwissVar',
                'slug' => 'swiss-var',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => ExpasyQueryType::flags([
                    ExpasyQueryType::UNIPROTID,
                    ExpasyQueryType::UNIPROTAC,
                ]),
                'url' => 'https://swissvar.expasy.org/',
                'meta_search_url' => 'https://web.expasy.org/cgi-bin/sib_geneva_search?resource=swissvar',
                'description' => '',
                'short_description' => 'Variants in UniProtKB entries',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::PROTEINS_AND_PROTEOMES,
            ],
            Resource::VENOMZONE => [
                'id' => Resource::VENOMZONE,
                'title' => 'VenomZone',
                'slug' => 'venom-zone',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => null,
                'url' => 'https://venomzone.expasy.org/',
                'meta_search_url' => 'https://web.expasy.org/cgi-bin/sib_geneva_search?resource=venomzone',
                'description' => '',
                'short_description' => 'Portal to venom protein UniProtKB entries',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::PROTEINS_AND_PROTEOMES,
            ],
            Resource::SWISSDOCK => [
                'id' => Resource::SWISSDOCK,
                'title' => 'SwissDock',
                'slug' => 'swissdock',
                'type' => ResourceTypesEnum::flags([ResourceTypesEnum::DB]),
                'query_type' => ExpasyQueryType::flags([
                    ExpasyQueryType::PDBID,
                ]),
                'url' => 'http://www.swissdock.ch/',
                'meta_search_url' => 'http://swissdock.vital-it.ch/search',
                'description' => '',
                'short_description' => 'SwissDock is a protein ligand docking server',
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => '',
                'pinned' => true,
                'activated' => true,
                'category_name' => ResourceCategorySeeder::STRUCTURAL_BIOLOGY,
            ],
        ];

        static $categories = [];
        foreach ($searchableResources as $searchableResource) {
            $categoryName = $searchableResource['category_name'];
            unset($searchableResource['category_name']);
            $resource = Resource::create($searchableResource);

            if (! array_key_exists($categoryName, $categories)) {
                $categories[$categoryName] = ResourceCategory::whereTitle($categoryName)->first()->id;
            }
            $resource->categories()->sync([$categories[$categoryName]]);
        }
    }

    public static function getRandomLicenseTypeId()
    {
        return null;
    }
}
