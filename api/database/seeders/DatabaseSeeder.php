<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        $this->call([
            ResourceCategorySeeder::class,
            ResourceLicenceTypeSeeder::class,
            MetaSearchResourcesSeeder::class,
            ResourcesSeeder::class,
        ]);
    }
}
