<?php

namespace Database\Seeders;

use App\Models\ResourceCategory;
use Illuminate\Database\Seeder;

class ResourceCategorySeeder extends Seeder
{
    const STRUCTURAL_BIOLOGY = 'Structural Biology';

    const SYSTEMS_BIOLOGY = 'Systems Biology';

    const GENES_AND_GENOMES = 'Genes & Genomes';

    const EVOLUTION_AND_PHYLOGENY = 'Evolution & Phylogeny';

    const PROTEINS_AND_PROTEOMES = 'Proteins & Proteomes';

    public function run(): void
    {
        $data = [
            ['title' => 'Genes & Genomes', 'parent' => null, 'icon' => 'gene'],
            ['title' => 'Genomics', 'parent' => 'Genes & Genomes', 'icon' => null],
            ['title' => 'Metagenomics', 'parent' => 'Genes & Genomes', 'icon' => null],
            ['title' => 'Transcriptomics', 'parent' => 'Genes & Genomes', 'icon' => null],
            ['title' => 'Proteins & Proteomes', 'parent' => null, 'icon' => 'protein'],
            ['title' => 'Evolution & Phylogeny', 'parent' => null, 'icon' => 'evolution'],
            ['title' => 'Evolution biology', 'parent' => 'Evolution & Phylogeny', 'icon' => null],
            ['title' => 'Population genetics', 'parent' => 'Evolution & Phylogeny', 'icon' => null],
            ['title' => 'Structural Biology', 'parent' => null, 'icon' => 'stru-biology'],
            ['title' => 'Drug design', 'parent' => 'Structural Biology', 'icon' => null],
            ['title' => 'Medicinal chemistry', 'parent' => 'Structural Biology', 'icon' => null],
            ['title' => 'Structural analysis', 'parent' => 'Structural Biology', 'icon' => null],
            ['title' => 'Systems Biology', 'parent' => null, 'icon' => 'sys-biology'],
            ['title' => 'Glycomics', 'parent' => 'Systems Biology', 'icon' => null],
            ['title' => 'Lipidomics', 'parent' => 'Systems Biology', 'icon' => null],
            ['title' => 'Metabolomics', 'parent' => 'Systems Biology', 'icon' => null],
            ['title' => 'Text mining & Machine learning', 'parent' => null, 'icon' => 'text-mining'],
        ];

        static $parent = [];
        foreach ($data as $category) {
            $newCategory = ['title' => $category['title'], 'icon' => $category['icon'], 'parent_id' => null];

            if ($category['parent'] !== null) {
                if (! array_key_exists($category['parent'], $parent)) {
                    $parent[$category['parent']] = ResourceCategory::whereTitle($category['parent'])->first();
                }
                $newCategory['parent_id'] = $parent[$category['parent']]->id;
            }

            ResourceCategory::firstOrCreate(['title' => $newCategory['title']], $newCategory);
        }
    }
}
