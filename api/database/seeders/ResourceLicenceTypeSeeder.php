<?php

namespace Database\Seeders;

use App\Models\ResourceLicenseType;
use Faker\Factory;
use Illuminate\Database\Seeder;

class ResourceLicenceTypeSeeder extends Seeder
{
    public function run(): void
    {
        $faker = Factory::create();
        foreach (ResourceLicenseType::LICENSES as $licence) {
            ResourceLicenseType::firstOrCreate([
                'link_label' => $licence,
            ], [
                'title' => $faker->name(),
                'link_url' => $faker->url,
            ]);
        }
    }
}
