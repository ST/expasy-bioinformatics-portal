<?php

namespace Database\Seeders;

use App\Enums\ResourceTypesEnum;
use App\Models\OntologyTerm;
use App\Models\Resource;
use App\Models\ResourceCategory;
use App\Models\ResourceLicenseType;
use Faker\Factory;
use Illuminate\Database\Seeder;

class ResourcesSeeder extends Seeder
{
    const RESOURCES_COUNT = 139;

    public function run(): void
    {
        $faker = Factory::create();
        for ($i = 0; $i < self::RESOURCES_COUNT; $i++) {
            $resource = Resource::create([
                'title' => $faker->text(20),
                'slug' => $faker->slug(3),
                'type' => ResourceTypesEnum::flags([rand(1, 3)]),
                'url' => $faker->url(),
                'meta_search_url' => $faker->url(),
                'media_url' => rand(0, 1) === 1 ? $faker->url() : null,
                'description' => $faker->realText(500),
                'short_description' => $faker->realText(60),
                'resource_license_type_id' => self::getRandomLicenseTypeId(),
                'group_info' => $faker->text(100),
                'pinned' => rand(0, 15) === 0,
                'activated' => rand(0, 8) !== 0,
            ]);

            $randParent = rand(1, 2);
            $resource->categories()->sync(self::getRandomResourceCategoryIds($randParent, rand($randParent, 5)));
            $resource->ontology_terms()->sync(self::getRandomOntologyTermIds());
        }
    }

    /**
     * Return a random licence type id
     *
     * @return |null
     */
    public static function getRandomLicenseTypeId()
    {
        return ResourceLicenseType::all()->pluck('id')->random(1)->first();
    }

    /**
     * Return an array of resource category ids
     *
     * @param  int  $categoryFamilyCount
     * @param  int  $childrenCount
     */
    public static function getRandomResourceCategoryIds($categoryFamilyCount = 2, $childrenCount = 3): array
    {
        $parentCategories = ResourceCategory::whereNull('parent_id')
            ->get()
            ->random($categoryFamilyCount);

        $childrenCategories = ResourceCategory::whereIn('parent_id', $parentCategories->pluck('id'))
            ->select('id', 'parent_id')
            ->get();

        if ($childrenCategories->count() > $childrenCount) {
            $childrenCategories = $childrenCategories->random($childrenCount);
        }

        $categoryIds = $childrenCategories->pluck('id');

        $parentCategories->each(function ($category) use ($categoryIds) {
            if (! $category->children()->exists()) {
                $categoryIds->push($category->id);
            }
        });

        return $categoryIds->toArray();
    }

    public static function getRandomOntologyTermIds(int $count = 15): array
    {
        return OntologyTerm::select('id')->get()->random($count)->pluck('id')->toArray();
    }
}
