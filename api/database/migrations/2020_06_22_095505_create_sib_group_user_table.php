<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSibGroupUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sib_group_user', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            $table->foreignId('sib_group_id');
            $table->foreignId('user_id');

            $table->foreign('sib_group_id')
                ->references('id')->on('sib_groups');
            $table->foreign('user_id')
                ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sib_group_user');
    }
}
