<?php

use App\Models\SibGroup;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGroupLdapIdentifiers extends Migration
{
    public function up(): void
    {
        if (! Schema::hasColumn('sib_groups', 'ldap_identifier')) {
            Schema::table('sib_groups', static function (Blueprint $table) {
                $table->string('ldap_identifier');
            });
        }

        $groupWithIds = [
            'ACGT' => 'Applied Computational Genomics Team',
            'AIIH' => 'Artificial Intelligence in Health',
            'ANEDC' => 'Artificial and Natural Evolutionary Development of Complexity',
            'BCB' => 'Biomathematics and Computational Biology',
            'BCF' => 'Bioinformatics Core Facility',
            'BI' => 'Biomedical Informatics',
            'BP' => 'Bioinformatics Proteogenomics',
            'BSB' => 'Bioinformatics Systems Biology',
            'BUGfri' => 'BUGFri',
            'BioMeXT' => 'BioMeXT Biomedical Information Extraction',
            'CAME' => 'Computer-aided Molecular Engineering',
            'CB' => 'Computational Biology',
            'CBC' => 'Computational Biology CoBi',
            'CBE' => 'Computational Biology ETH',
            'CBG' => 'Computational Biology Group',
            'CBR' => 'Computational Biochemistry Research',
            'CBU' => 'Clinical Bioinformatics Unit',
            'CCB' => 'Computational Cancer Biology',
            'CCG' => 'Computational Cancer Genomics',
            'CE' => 'Computational Evolution',
            'CEBG' => 'Computational Evolutionary Biology and Genomics',
            'CEG' => 'Computational Evolutionary Genomics',
            'CEOC' => 'Computational Epigenetics of Cancer',
            'CI4CB' => 'Computational Intelligence for Computational Biology',
            'CLIB' => 'Clinical Bioinformatics',
            'CMPG' => 'Computational and Molecular Population Genetics',
            'CP' => 'Computational Phylogenetics',
            'CSB' => 'Computational Systems Biology',
            'CSBI' => 'Computational Structural Biology IRB',
            'CSO' => 'Computational Systems Oncology',
            'CSTB' => 'Computational Structural Biology',
            'Calipho' => 'CALIPHO',
            'Core-IT' => 'Core-IT',
            'DBMBCF' => 'DBM Bioinformatics Core Facility',
            'DHP' => 'Digital Humanities Plus',
            'DNACM' => 'DNA and Chromosome Modelling',
            'EB' => 'Evolutionary Bioinformatics',
            'EFG' => 'Evolutionary-Functional Genomics',
            'EG' => 'Evolutionary Genomics',
            'EM' => 'Evolutionary Microbiology',
            'ESB' => 'Evolutionary Systems Biology',
            'EvoNeuro' => 'EvoNeuro',
            'FGCZ' => 'Functional Genomics Center Zurich',
            'FMICB' => 'FMI Computational Biology',
            'GCT' => 'Genomics of Complex Traits',
            'GSB' => 'Genome Systems Biology',
            'HPG' => 'Host-Pathogen Genomics',
            'IBU' => 'Interfaculty Bioinformatics Unit',
            'IORBCU' => 'IOR Bioinformatics Core Unit',
            'LBM' => 'Laboratory for Biomolecular Modeling',
            'LPDI' => 'Laboratory of Protein Design and Immunoengineering',
            'LSBG' => 'Laboratory of Systems Biology and Genetics',
            'MA' => 'Molecular Allergology',
            'MDS' => 'Medical Data Science',
            'ME' => 'Microbial Evolution',
            'MLACBL' => 'Machine Learning and Computational Biology Lab',
            'MM' => 'Molecular Modelling',
            'MR' => 'Microbiome Research',
            'OsiriX' => 'OsiriX',
            'PCPAC' => 'PHRT Clinical Proteotype Analysis Center',
            'PGG' => 'Population Genetics and Genomics',
            'PHI' => 'Personalized Health Informatics',
            'PIF' => 'Proteome Informatics FGCZ',
            'PIG' => 'Proteome Informatics',
            'RNARN' => 'RNA Regulatory Networks',
            'SAPG' => 'Systems and Population Genetics',
            'SB' => 'Statistical Bioinformatics',
            'SC' => 'Scientific Computing',
            'SCEB' => 'Statistical and Computational Evolutionary Biology',
            'SG' => 'Statistical Genetics',
            'SIS' => 'Scientific IT Services',
            'SL' => 'Snijder Lab',
            'SPC' => 'Scientific and Parallel Computing',
            'SSSI' => 'Service and Support for Science IT',
            'STM' => 'SIB Text Mining',
            'SwissProt' => 'Swiss-Prot',
            'TO' => 'Theoretical Oncogenomics',
            'Vital-IT' => 'Vital-IT',
            'dummy-group' => 'Dummy Group',
            'sciCORE' => 'sciCORE',
        ];

        foreach ($groupWithIds as $id => $group) {
            SibGroup::where('name', $group)->update(['ldap_identifier' => $id]);
        }
    }

    public function down(): void
    {
        //
    }
}
