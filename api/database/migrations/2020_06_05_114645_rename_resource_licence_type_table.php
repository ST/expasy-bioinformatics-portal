<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameResourceLicenceTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('resources', function (Blueprint $table) {
            $table->dropForeign('resources_resource_licence_type_id_foreign');
            $table->renameColumn('resource_licence_type_id', 'resource_license_type_id');
        });

        Schema::rename('resource_licence_types', 'resource_license_types');

        Schema::table('resources', function (Blueprint $table) {
            $table->foreign('resource_license_type_id')
                ->references('id')
                ->on('resource_license_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        throw new Exception('This migration cannot be reverted');
    }
}
