<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateSearchUrl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('resources', function (Blueprint $table) {
            $table->renameColumn('search_url', 'meta_search_url');
        });

        Schema::table('resources', function (Blueprint $table) {
            $table->string('meta_search_url')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('resources', function (Blueprint $table) {
            $table->renameColumn('meta_search_url', 'search_url');
        });

        Schema::table('resources', function (Blueprint $table) {
            $table->string('meta_search_url')->nullable(false)->change();
        });
    }
}
