<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug');

            $table->integer('type');
            $table->string('url');
            $table->string('search_url');
            $table->text('description');
            $table->string('short_description');
            $table->string('media_url')->nullable();
            $table->text('group_info');

            $table->foreignId('resource_licence_type_id')->nullable();
            $table->boolean('activated')->default(false);

            $table->timestamps();

            $table->foreign('resource_licence_type_id')
                ->references('id')->on('resource_licence_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resources');
    }
}
