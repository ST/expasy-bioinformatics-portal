<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKeycloakIdOnUserTable extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('keycloak_id')->nullable()->unique();

            $table->dropColumn('password');
            $table->dropColumn('email_verified_at');
            $table->dropColumn('email');
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('keycloak_id');

            $table->string('password');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('email');
        });
    }
}
