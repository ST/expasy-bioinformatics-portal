<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResourceResourceCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_resource_category', function (Blueprint $table) {
            $table->id();
            $table->foreignId('resource_id');
            $table->foreignId('resource_category_id');

            $table->foreign('resource_id')
                ->references('id')->on('resources');
            $table->foreign('resource_category_id')
                ->references('id')->on('resource_categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_resource_category');
    }
}
