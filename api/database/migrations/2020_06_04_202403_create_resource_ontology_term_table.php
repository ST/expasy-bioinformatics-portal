<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResourceOntologyTermTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ontology_term_resource', function (Blueprint $table) {
            $table->id();
            $table->foreignId('resource_id');
            $table->foreignId('ontology_term_id');

            $table->foreign('resource_id')
                ->references('id')->on('resources')->onDelete('cascade');
            $table->foreign('ontology_term_id')
                ->references('id')->on('ontology_terms')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ontology_term_resource');
    }
}
