<?php

use App\Models\SibGroup;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSibGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sib_groups', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name')->unique();
        });

        $groups = [
            ['name' => 'Applied Computational Genomics Team'],
            ['name' => 'Artificial Intelligence in Health'],
            ['name' => 'Artificial and Natural Evolutionary Development of Complexity'],
            ['name' => 'BUGFri'],
            ['name' => 'BioMeXT Biomedical Information Extraction'],
            ['name' => 'Bioinformatics Core Facility'],
            ['name' => 'Bioinformatics Proteogenomics'],
            ['name' => 'Bioinformatics Systems Biology'],
            ['name' => 'Bioinformatics and Biostatistics Core Facility'],
            ['name' => 'Biomathematics and Computational Biology'],
            ['name' => 'Biomedical Informatics - Gunnar Rätsch'],
            ['name' => 'CALIPHO'],
            ['name' => 'Clinical Bioinformatics '],
            ['name' => 'Clinical Bioinformatics Unit'],
            ['name' => 'Computational Biochemistry Research'],
            ['name' => 'Computational Biology'],
            ['name' => 'Computational Biology CoBi '],
            ['name' => 'Computational Biology ETH'],
            ['name' => 'Computational Biology Group'],
            ['name' => 'Computational Cancer Biology'],
            ['name' => 'Computational Cancer Genomics'],
            ['name' => 'Computational Epigenetics of Cancer'],
            ['name' => 'Computational Evolution'],
            ['name' => 'Computational Evolutionary Biology and Genomics'],
            ['name' => 'Computational Evolutionary Genomics'],
            ['name' => 'Computational Intelligence for Computational Biology'],
            ['name' => 'Computational Phylogenetics'],
            ['name' => 'Computational Structural Biology'],
            ['name' => 'Computational Structural Biology IRB '],
            ['name' => 'Computational Systems Biology'],
            ['name' => 'Computational Systems Oncology'],
            ['name' => 'Computational and Molecular Population Genetics'],
            ['name' => 'Computer-aided Molecular Engineering'],
            ['name' => 'Core-IT'],
            ['name' => 'DBM Bioinformatics Core Facility'],
            ['name' => 'DNA and Chromosome Modelling'],
            ['name' => 'Digital Humanities Plus'],
            ['name' => 'EvoNeuro'],
            ['name' => 'Evolutionary Bioinformatics'],
            ['name' => 'Evolutionary Genomics'],
            ['name' => 'Evolutionary Microbiology'],
            ['name' => 'Evolutionary Systems Biology'],
            ['name' => 'Evolutionary-Functional Genomics'],
            ['name' => 'FMI Computational Biology'],
            ['name' => 'Functional Genomics Center Zurich'],
            ['name' => 'Genome Systems Biology'],
            ['name' => 'Genomics of Complex Traits'],
            ['name' => 'Host-Pathogen Genomics '],
            ['name' => 'IOR Bioinformatics Core Unit'],
            ['name' => 'IT Services'],
            ['name' => 'Interfaculty Bioinformatics Unit'],
            ['name' => 'Laboratory for Biomolecular Modeling'],
            ['name' => 'Laboratory of Protein Design and Immunoengineering'],
            ['name' => 'Laboratory of Systems Biology and Genetics'],
            ['name' => 'Machine Learning and Computational Biology Lab'],
            ['name' => 'Medical Data Science'],
            ['name' => 'Microbial Evolution'],
            ['name' => 'Microbiome Research'],
            ['name' => 'Molecular Allergology'],
            ['name' => 'Molecular Modelling'],
            ['name' => 'OsiriX'],
            ['name' => 'PHRT Clinical Proteotype Analysis Center'],
            ['name' => 'Personalized Health Informatics'],
            ['name' => 'Population Genetics and Genomics'],
            ['name' => 'Proteome Informatics'],
            ['name' => 'RNA Regulatory Networks'],
            ['name' => 'SIB Text Mining'],
            ['name' => 'Scientific Computing'],
            ['name' => 'Scientific and Parallel Computing'],
            ['name' => 'Service and Support for Science IT'],
            ['name' => 'Snijder Lab'],
            ['name' => 'Statistical Bioinformatics '],
            ['name' => 'Statistical Genetics'],
            ['name' => 'Statistical and Computational Evolutionary Biology'],
            ['name' => 'Swiss-Prot'],
            ['name' => 'Systems and Population Genetics'],
            ['name' => 'Theoretical Oncogenomics'],
            ['name' => 'Vital-IT'],
            ['name' => 'sciCORE'],
        ];

        SibGroup::insert($groups);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sib_groups');
    }
}
