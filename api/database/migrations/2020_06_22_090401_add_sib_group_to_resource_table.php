<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSibGroupToResourceTable extends Migration
{
    public function up()
    {
        Schema::table('resources', function (Blueprint $table) {
            $table->foreignId('sib_group_id')->nullable();
            $table->foreign('sib_group_id')->references('id')->on('sib_groups');
        });
    }

    public function down()
    {
        Schema::table('resources', function (Blueprint $table) {
            $table->dropForeign('sib_group_id');
            $table->dropColumn('sib_group_id');
        });
    }
}
