<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CascadeDeleteResourceCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('resource_resource_category', function (Blueprint $table) {
            $table->dropForeign('resource_resource_category_resource_id_foreign');
            $table->foreign('resource_id')
                ->references('id')
                ->on('resources')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('resource_resource_category', function (Blueprint $table) {
            $table->dropForeign('resource_resource_category_resource_id_foreign');
            $table->foreign('resource_id')
                ->references('id')
                ->on('resources');
        });
    }
}
