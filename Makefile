# -----------------------------------------------------------------------------
# Install
# -----------------------------------------------------------------------------
# Install the DDEV local environment
ddev-install:
	ddev start
	make install:env
	ddev composer install
	ddev artisan nova:publish
	ddev artisan key:generate --ansi
	ddev artisan storage:link
	chmod -R 777 api/bootstrap/cache
	chmod -R 777 api/storage
	ddev artisan migrate
	ddev artisan ontology:refresh-terms
	ddev artisan db:seed
	ddev artisan scout:import "App\Models\Resource"
	ddev restart

# Copy the environment files
install\:env:
	cp .env.api.example api/.env
	cp .env.client.example client/.env

# Install composer dependencies
install\:composer:
	ddev php composer install

install\:artisan\:publish:
	ddev php artisan nova:publish

# Generate a Laravel application key
install\:key:
	ddev php artisan key:generate --ansi

# Add permissions for Laravel folders
install\:permissions:
	chmod -R 777 api/bootstrap/cache
	chmod -R 777 api/storage

# Generate a Laravel storage symlink
install\:storage:
	ddev php artisan storage:link

# -----------------------------------------------------------------------------
# Database
# -----------------------------------------------------------------------------

db\:migrate:
	ddev php artisan migrate

db\:migrate\:fresh:
	ddev php artisan migrate:fresh

db\:rollback:
	ddev php artisan migrate:rollback

db\:seed:
	ddev php artisan db:seed

db\:dump:
	ddev exec mariadb mysqldump -u expasy -pexpasy expasy > docker/mariadb/dumps/dump.sql

# -----------------------------------------------------------------------------
# Scout
# -----------------------------------------------------------------------------

scout\:import:
	ddev php artisan scout:import "App\Models\Resource"



# Run tinker
tinker:
	ddev php artisan tinker

# -----------------------------------------------------------------------------
# Ontology
# -----------------------------------------------------------------------------

ontology\:refresh-terms:
	ddev php artisan ontology:refresh-terms
