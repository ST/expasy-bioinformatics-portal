# ExPASy

[Bioinformatics Resource Portal](https://expasy.org) by the Swiss Institute of Bioinformatics (SIB).

## Getting Started

The project uses [Laravel](https://laravel.com/) and [Laravel Nova](https://nova.laravel.com/) for
the server side and [Nuxt](https://nuxtjs.org/) for the client side. Docker is used for setting up the
development environment.

For the sake of simplicity, the project is organized as a mono-repository. The client-side (Nuxt)
can be found in the `client` folder and the server-side (Laravel) can be found in the `api` folder.
Despite being in the same repository, each are operating separately and have their own configurations.
You can find out more in their respective `README.md` files.

## Prerequisites

 - Make sure you have already installed [Docker Engine](https://docs.docker.com/get-docker/),
 - [Docker Compose](https://docs.docker.com/compose/install/)
 - Install DDEV
 - PHP and [Composer](https://getcomposer.org/) to be installed globally for the deployment.

## Installation

**1. Clone the repository and enter its folder**

```bash
$ git clone git@gitlab.sib.swiss:ST/expasy-bioinformatics-portal.git folder-of-your-choice
$ cd folder-of-your-choice
```

**2. Laravel Nova composer package authentication**

```bash
$ cd api && composer config http-basic.nova.laravel.com nova_username nova_password && cd ..
```

**3. Run the installation script**

```bash
$ make ddev-install
# The script will ask for your sudo user password
```

The base url is `https://expasy-bioinformatics-portal.ddev.site/` which exposes Nuxt.

ToDO: The server will proxy all requests with the `/api` prefix to Laravel and `/admin` to Laravel Nova.
For the moment the Laravel app is served to port 4433 https://expasy-bioinformatics-portal.ddev.site:4443/

*If you see the 502 error page, just wait a bit for `npm install && npm start` process to be
finished (`make docker:logs:node` to check the status of the process).*

**Note:** The Docker images definitions can be found in the `docker` folder which have sub-folders
for each service running in the development environment. The services are defined inside the
`docker-compose.yml` file in the root folder.

## Database

You can create a dump of the database with the `make db:dump` command. The `Makefile` also offers
other commands for you to use.

A database administration panel (PHPMyAdmin) is also available at `https://pma.expasy.docker.test`.
You can log in with the following credentials: **Username**: `expasy` and **Password**: `expasy`.

Seeds exist if you need development data and can be ran with the command `php artisan db:seed` in the `api` folder.

## Deployment

Deployment uses [Deployer](https://deployer.org/) as deployment tool. The main configuration file is the following: `deploy.php`

Recipes were made in order to configure the deployment for Laravel and Nuxt and can be found in `recepie/laravel.php`
and `receipe/nuxt.php`.

In order for the deployment to succeed, you must, beforehand, have the .env files set up in the server. They should be
placed in `shared/api/.env` and `shared/client/.env`. Examples can be found in files `env.{api|client}.example`.

After each deployment, environment file is cached in the application. If you decide to modify a value, you must run a
`php artisan config:cache` in the `api` folder in order to ensure the new values will be read.


#### Deployment command

```bash
$ vendor/bin/dep deploy <staging|prod>
```

#### PM2

[PM2](https://pm2.keymetrics.io/docs/usage/quick-start/) must be installed globally. Once installed, an ecosystem config
file must be placed in the `shared/client/` folder with the following data:

```js
module.exports = {
    apps: [
        {
            name: 'nuxt',
            cwd: '../../current/client',
            script: './node_modules/nuxt-start/bin/nuxt-start.js',
            env: {
                'HOST': 'localhost',
                'PORT': 3000,
                'NODE_ENV': 'production',
            },
        }
    ]
};
```

Once at least one deployment was made (which must have failed if it's the first), you can now go to `shared/client` and
run `pm2 start`. Know that a systemd config can be made to ensure pm2 runtime through server restart.

## Meilisearch
example url: https://expasy-bioinformatics-portal.ddev.site/search/blast
use meilisearch search

#### Manually indexing data

```bash
$ ddev artisan scout:import "App\Models\Resource"
```

