import path from 'path';
import SpriteLoaderPlugin from 'svg-sprite-loader/plugin';
import axios from 'axios';
require('dotenv').config();

export default {
  // --------------------------------------------------------------------------
  // Head
  // --------------------------------------------------------------------------

  head: {
    titleTemplate: (titleChunk) => {
      return titleChunk
        ? `${titleChunk} - SIB Swiss Institute of Bioinformatics | Expasy`
        : 'SIB Swiss Institute of Bioinformatics | Expasy';
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: `Operated by the SIB Swiss Institute of Bioinformatics, Expasy, the Swiss Bioinformatics Resource Portal, provides access to scientific databases and software tools in different areas of life sciences.`,
      },
      { name: 'google-site-verification', content: 'wW27Qr93sFIEvPN3uocwfHWDLqMkl1yRN1dtmrJNjyY' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.bunny.net/css?family=Source+Sans+Pro:wght@400;600;700&display=swap',
      },
    ],
    htmlAttrs: {
      lang: 'en',
      class: 'font-body antialiased text-neutral-900',
    },
  },

  // --------------------------------------------------------------------------
  // Customization
  // --------------------------------------------------------------------------

  loading: { color: '#E70100' },

  // --------------------------------------------------------------------------
  // Plugins and their configurations
  // --------------------------------------------------------------------------

  plugins: [
    { src: '~/plugins/polyfills.js' },
    { src: '~/plugins/components.js' },
    { src: '~/plugins/services.js' },
    { src: '~/plugins/vue-matomo.js', ssr: false },
  ],

  // --------------------------------------------------------------------------
  // Server Middleware
  // --------------------------------------------------------------------------

  serverMiddleware: ['~/middleware/redirections.js'],

  // --------------------------------------------------------------------------
  // Build Modules
  // --------------------------------------------------------------------------

  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  tailwindcss: {
    cssPath: '~/assets/css/main.css',
  },

  // --------------------------------------------------------------------------
  // Modules
  // --------------------------------------------------------------------------

  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    // Doc: https://github.com/nuxt-community/sentry-module
    '@nuxtjs/sentry',
    // Doc: https://github.com/nuxt-community/sitemap-module
    '@nuxtjs/sitemap',
  ],

  sentry: {
    dsn: process.env.SENTRY_DSN || false,
    config: {
      environment: process.env.SENTRY_ENVIRONMENT,
    },
  },

  sitemap: {
    hostname: process.env.BASE_URL,
    gzip: true,
    defaults: {
      changefreq: 'weekly',
      priority: 0.6,
      lastmod: new Date(),
    },
    exclude: ['/terms-of-use', '/chat'],
    routes: async () => {
      const host = process.env.API_URL;

      // if (process.env.NODE_ENV === 'production') {
      //   host = 'https://expasy-prod8.vital-it.ch/api/v1';
      // } else if (process.env.NODE_ENV === 'local') {
      //   host = 'https://expasy-bioinformatics-portal.ddev.site/api/v1';
      // } else {
      //   host = process.env.API_URL;
      // }

      const response = await axios.get(`${host}/resources`);

      return [
        {
          url: '/',
          changefreq: 'daily',
          priority: 1,
          lastmod: new Date(),
        },
        ...response.data.map((resource) => `/resources/${resource.slug}`),
      ];
    },
  },

  // --------------------------------------------------------------------------
  // Build Configuration
  // --------------------------------------------------------------------------

  build: {
    extractCSS: process.env.NODE_ENV === 'production',

    // transpile: ['chusho'],

    plugins: [new SpriteLoaderPlugin({ plainSprite: true })],

    postcss: {
      preset: {
        stage: 2,
      },
      postcssOptions: {
        plugins: {
          'postcss-import': true,
        },
      },
    },

    extend(config, ctx) {
      // Prevent generic SVG loader from taking care of SVG icons going in the sprite
      config.module.rules[9].exclude = [path.resolve('./assets/icons')];

      // Load all icons into a single sprite
      config.module.rules.push({
        test: /\.svg$/,
        include: path.resolve('./assets/icons'),
        loader: 'svg-sprite-loader',
        options: {
          extract: true,
          spriteFilename: 'img/icons.[contenthash:base64:8].svg',
        },
      });
    },
  },
};
