export const state = () => ({
  error: '',
});

export const mutations = {
  setError(state, error) {
    state.error = error;
  },
};

export const actions = {
  setTooManyRequestsError({ commit }) {
    commit(
      'setError',
      'Too many requests were made in a too short period of time. Please wait a moment before making a new query.'
    );
  },

  clearError({ commit }) {
    commit('setError', '');
  },
};
