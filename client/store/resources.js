import { shuffle } from '@/assets/js/utils/array';

export const state = () => ({
  resources: [],
});

export const mutations = {
  setResources(state, resources) {
    state.resources = resources;
  },
};

export const getters = {
  databasesForCategories: (state) => (category) => {
    return state.resources.filter((resource) => {
      return resource.categories.some((localCategory) => localCategory.title === category);
    });
  },
};

export const actions = {
  getResources({ commit }) {
    return this.$resourcesService.getResources().then((resources) => {
      resources.forEach((resource) => {
        resource.typesAndCategories = [
          ...resource.categories.map((category) => category.id),
          ...resource.types.map((type) => type.id),
        ];
      });

      commit('setResources', shuffle(resources));
    });
  },
};
