export const state = () => ({
  categories: [],
  types: [],
  filters: [],
});

export const mutations = {
  setCategories(state, categories) {
    state.categories = categories;
  },

  setTypes(state, types) {
    state.types = types;
  },

  addFilter(state, filter) {
    state.filters.push(filter.id);

    // If the filter has children, also add them
    filter.children &&
      filter.children.length &&
      filter.children.forEach((item) => {
        state.filters.push(item.id);
      });
  },

  removeFilter(state, filter) {
    state.filters = state.filters.filter((localFilter) => localFilter !== filter.id);

    // If the filter has children, also remove them
    if (filter.children && filter.children.length) {
      state.filters = state.filters.filter((localFilter) => !filter.children.some((child) => child.id === localFilter));
    }
  },
};

export const actions = {
  getFilters({ commit }) {
    return this.$resourcesFilters.getResourcesFilters().then((resourcesFilters) => {
      commit('setCategories', resourcesFilters.categories);
      commit('setTypes', resourcesFilters.types);
    });
  },
};

export const getters = {
  filterIsActive: (state) => (filter) => {
    return state.filters.includes(filter.id);
  },

  hasActiveFilter:
    (state) =>
    (filters = []) => {
      return state.filters && state.filters.length ? filters.some((filter) => state.filters.includes(filter)) : true;
    },
};
