export const strict = process.env.NODE_ENV !== 'production';

export default {
  actions: {
    async nuxtServerInit({ dispatch }) {
      await dispatch('resources/getResources');
      await dispatch('resources-filters/getFilters');
    },
  },
};
