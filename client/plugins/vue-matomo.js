// https://forum.matomo.org/t/tag-manager-in-nuxt-application/42103/7

import Vue from 'vue';
import VueMatomo from 'vue-matomo';

export default ({ app }) => {
  Vue.use(VueMatomo, {
    router: app.router,
    host: 'https://matomo.sib.swiss',
    siteId: 2,
    trackInitialView: true,
    requireConsent: false,
    /** Other configuration options **/
  });
};
