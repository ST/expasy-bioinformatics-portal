import Vue from 'vue';

import AppIcon from '~/components/ui/AppIcon.vue';
import AppContainer from '~/components/ui/AppContainer.vue';
import AppSection from '~/components/ui/AppSection.vue';
import AppFlex from '~/components/ui/AppFlex.vue';
import AppFlexItem from '~/components/ui/AppFlexItem.vue';
import AppHeading from '~/components/ui/AppHeading.vue';
import AppTooltip from '~/components/ui/AppTooltip.vue';
import AppLoader from '~/components/ui/AppLoader.vue';
import AppRichText from '~/components/ui/AppRichText.vue';

Vue.component('AppIcon', AppIcon);
Vue.component('AppContainer', AppContainer);
Vue.component('AppSection', AppSection);
Vue.component('AppFlex', AppFlex);
Vue.component('AppFlexItem', AppFlexItem);
Vue.component('AppHeading', AppHeading);
Vue.component('AppTooltip', AppTooltip);
Vue.component('AppRichText', AppRichText);
Vue.component('AppLoader', AppLoader);
