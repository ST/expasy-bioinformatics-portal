import createResourcesFiltersService from '~/services/resources-filters';
import createResourcesService from '~/services/resources';
import createSearchService from '~/services/search';

export default ({ $axios }, inject) => {
  if (process.server && process.env.NODE_ENV === 'local') {
    $axios.setBaseURL('https://ddev-expasy-bioinformatics-portal-web/api/v1');
  } else {
    $axios.setBaseURL(process.env.API_URL);
  }

  // Injecting the services into the context
  inject('resourcesFilters', createResourcesFiltersService($axios)());
  inject('resourcesService', createResourcesService($axios)());
  inject('searchService', createSearchService($axios)());
};
