let resourceCancel;
const databaseCancel = {};

export default ($axios) => () => ({
  getSearchResources(query, type) {
    resourceCancel && resourceCancel();

    let url = `/resources/search/?query=${query}`;
    url = type ? `${url}&type=${type}` : url;

    return $axios.$get(url, {
      cancelToken: new $axios.CancelToken(function executor(c) {
        resourceCancel = c;
      }),
    });
  },

  getSearchDatabase(id, query) {
    databaseCancel[id] && databaseCancel[id]();

    return $axios.$get(`/resources/${id}/search/?query=${query}`, {
      cancelToken: new $axios.CancelToken(function executor(c) {
        databaseCancel[id] = c;
      }),
    });
  },
});
