import https from 'https';

const agent = new https.Agent({
  rejectUnauthorized: false,
});

export default ($axios) => () => ({
  getResources() {
    return $axios.$get('/resources', { httpsAgent: agent });
  },

  getResource(slug) {
    return $axios.$get(`/resources/?schema=full&slug=${slug}`, { httpsAgent: agent });
  },
});
