import https from 'https';

const agent = new https.Agent({
  rejectUnauthorized: false,
});

export default ($axios) => () => ({
  getResourcesFilters() {
    return $axios.$get('/resources-filters', { httpsAgent: agent });
  },
});
