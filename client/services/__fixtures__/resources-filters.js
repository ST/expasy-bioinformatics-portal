export const resourcesFilters = {
  categories: [
    {
      id: 1,
      icon: 'gene',
      title: 'Genes & Genomes',
      items: [
        { id: 2, title: 'Genomics' },
        { id: 3, title: 'Metagenomics' },
        { id: 4, title: 'Transcriptomics' },
      ],
    },
    {
      id: 5,
      icon: 'protein',
      title: 'Proteins & Proteomes',
    },
    {
      id: 6,
      icon: 'evolution',
      title: 'Evolution & Phylogeny',
      items: [
        { id: 7, title: 'Evolutionary biology' },
        { id: 8, title: 'Population genetics' },
      ],
    },
    {
      id: 9,
      icon: 'stru-biology',
      title: 'Structural biology',
      items: [
        { id: 10, title: 'Drug design' },
        { id: 11, title: 'Medicinal chemistry' },
        { id: 12, title: 'Structural analysis' },
      ],
    },
    {
      id: 13,
      icon: 'sys-biology',
      title: 'Systems Biology',
      items: [
        { id: 14, title: 'Glycomics' },
        { id: 15, title: 'Lipidomics' },
        { id: 16, title: 'Metabolomics' },
      ],
    },
    {
      id: 17,
      icon: 'text-mining',
      title: 'Text mining & machine learning',
    },
  ],
  types: [
    {
      id: 'db',
      icon: 'db',
      title: 'Database',
    },
    {
      id: 'tool',
      icon: 'tool',
      title: 'Software tool',
    },
  ],
};
