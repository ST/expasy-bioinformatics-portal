export const resources = [
  {
    id: 1,
    title: 'amet justo',
    slug: 'amet-justo',
    short_description: 'auctor gravida sem praesent id massa id nisl venenatis',
    categories: [
      { id: 1, icon: 'gene', title: 'Genes & Genomes' },
      { id: 2, icon: 'gene', title: 'Genomics' },
    ],
    types: [{ id: 'db', icon: 'db', title: 'Database' }],
  },
  {
    id: 2,
    title: 'hac habitasse platea dictumst',
    slug: 'hac-habitasse-platea-dictumst',
    short_description: 'donec diam neque vestibulum eget vulputate ut ultrices vel augue',
    pinned: true,
    categories: [
      { id: 1, icon: 'gene', title: 'Genes & Genomes' },
      { id: 2, icon: 'gene', title: 'Genomics' },
    ],
    types: [{ id: 'db', icon: 'db', title: 'Database' }],
  },
  {
    id: 3,
    title: 'ut suscipit',
    slug: 'ut-suscipit',
    short_description: 'et ultrices posuere cubilia curae donec pharetra magna vestibulum',
    categories: [
      { id: 3, icon: 'gene', title: 'Metagenomics' },
      { id: 4, icon: 'gene', title: 'Transcriptomics' },
    ],
    types: [{ id: 'tool', icon: 'tool', title: 'Software Tool' }],
  },
  {
    id: 4,
    title: 'pede justo',
    slug: 'pede-justo',
    short_description: 'gravida sem praesent id massa id nisl venenatis lacinia aenean',
    categories: [
      { id: 5, icon: 'protein', title: 'Proteins & Proteomes' },
      { id: 6, icon: 'evolution', title: 'Evolution & Phylogeny' },
    ],
    types: [{ id: 'tool', icon: 'tool', title: 'Software Tool' }],
  },
  {
    id: 5,
    title: 'eros suspendisse accumsan',
    slug: 'eros-suspendisse-accumsan',
    short_description: 'ligula nec sem duis aliquam convallis nunc proin at turpis',
    categories: [
      { id: 6, icon: 'evolution', title: 'Evolution & Phylogeny' },
      { id: 7, icon: 'evolution', title: 'Evolutionary biology' },
    ],
    types: [
      { id: 'db', icon: 'db', title: 'Database' },
      { id: 'tool', icon: 'tool', title: 'Software Tool' },
    ],
  },
  {
    id: 6,
    title: 'congue elementum',
    slug: 'congue-elementum',
    short_description: 'augue luctus tincidunt nulla mollis molestie lorem quisque ut',
    categories: [
      { id: 9, icon: 'stru-biology', title: 'Structural biology' },
      { id: 10, icon: 'stru-biology', title: 'Drug design' },
      { id: 11, icon: 'stru-biology', title: 'Medicinal chemistry' },
      { id: 12, icon: 'stru-biology', title: 'Structural analysis' },
    ],
    types: [
      { id: 'db', icon: 'db', title: 'Database' },
      { id: 'tool', icon: 'tool', title: 'Software Tool' },
    ],
  },
  {
    id: 7,
    title: 'quis augue luctus tincidunt',
    slug: 'quis-augue-luctus-tincidunt',
    short_description: 'erat curabitur gravida nisi at nibh in hac habitasse platea',
    pinned: true,
    categories: [
      { id: 14, icon: 'sys-biology', title: 'Glycomics' },
      { id: 15, icon: 'sys-biology', title: 'Lipidomics' },
      { id: 16, icon: 'sys-biology', title: 'Metabolomics' },
      { id: 17, icon: 'text-mining', title: 'Text mining & machine learning' },
    ],
    types: [{ id: 'db', icon: 'db', title: 'Database' }],
  },
  {
    id: 8,
    title: 'pellentesque viverra',
    slug: 'pellentesque-viverra',
    short_description: 'suscipit ligula in lacus curabitur at ipsum ac tellus semper',
    pinned: true,
    categories: [
      { id: 16, icon: 'sys-biology', title: 'Metabolomics' },
      { id: 17, icon: 'text-mining', title: 'Text mining & machine learning' },
    ],
    types: [{ id: 'tool', icon: 'tool', title: 'Software Tool' }],
  },
  {
    id: 9,
    title: 'morbi quis tortor id',
    slug: 'morbi-quis-tortor-id',
    short_description: 'magnis dis parturient montes nascetur ridiculus mus vivamus',
    categories: [
      { id: 16, icon: 'sys-biology', title: 'Metabolomics' },
      { id: 17, icon: 'text-mining', title: 'Text mining & machine learning' },
    ],
    types: [{ id: 'db', icon: 'db', title: 'Database' }],
  },
  {
    id: 10,
    title: 'nonummy integer non',
    slug: 'nonummy-integer-non',
    short_description: 'ante nulla justo aliquam quis turpis eget elit sodales',
    categories: [
      { id: 16, icon: 'sys-biology', title: 'Metabolomics' },
      { id: 17, icon: 'text-mining', title: 'Text mining & machine learning' },
    ],
    types: [
      { id: 'db', icon: 'db', title: 'Database' },
      { id: 'tool', icon: 'tool', title: 'Software Tool' },
    ],
  },
];

export const resource = {
  id: 1,
  title: 'amet justo',
  slug: 'amet-justo',
  url: 'https://epd.epfl.ch/index.php',
  description:
    'The Eukaryotic Promoter Database is an annotated non-redundant collection of\neukaryotic POL II promoters, for which the transcription start site has been determined experimentally.\nAccess to promoter sequences is provided by pointers to positions in nucleotide sequence entries.\nThe annotation part of an entry includes description of the initiation site mapping data,\ncross-references to other databases, and bibliographic references.',
  short_description: 'Collection of eukaryotic promoters',
  group_info: `<div>This is the group info with a <a href='https://epd.epfl.ch/index.php'>link</a></div>`,
  license_type: {
    id: 5,
    logo: 'cc-by-nc',
    title: 'Creative Commons',
    link_url: 'https://creativecommons.org/licenses/by/4.0/',
    link_label: 'CC-BY NC',
  },
  categories: [
    { id: 1, icon: 'gene', title: 'Genes & Genomes' },
    { id: 2, icon: 'gene', title: 'Genomics' },
    { id: 17, icon: 'text-mining', title: 'Text mining & machine learning' },
  ],
  types: [{ id: 'db', icon: 'db', title: 'Database' }],
};
