describe('Portal', () => {
  // cookie is now removed because we moved to matomo
  // it('Display cookie consent', () => {
  //   cy.visit('/');
  //   cy.get('.cookie');
  //   cy.get('.cookie__btn > .btn').click();
  //   cy.get('.cookie').should('not.exist');
  // });

  it('Facet selection filters the list of resources', () => {
    cy.visit('/');
    cy.log('click on the first facet which is "Genes & Genomes"')
      .get(':nth-child(1) > .resources-filters__group > .font-bold > label > .checkbox__label')
      .click();
  });

  it('Searches and find results', () => {
    cy.visit('/');
    cy.get('#search').type('blast{enter}');
    cy.get('.page-main').contains('UniProt BLAST');

    cy.log('Find "You can also query..."')
      .get('.section-no-hit > .framed')
      .contains('You can also query "blast" into a selection of SIB databases in parallel')
      .click();

    cy.log('Find "You can also query..."')
      .get(':nth-child(2) > .framed')
      .contains('“blast” queried in 19 SIB databases');
    cy.get('.search-page__title').should('have.length', 5);
    cy.get('[test="swiss_model"]').contains('hits');
  });

  it('Resource', () => {
    cy.visit('/resources/swissorthology');
    cy.log('Find "What you can do with this resource"')
      .get('.resource-page__ontologie > :nth-child(1)')
      .then((container) => cy.get(container).contains('Annotation'));

    cy.log('Find "Browse these keywords in Expasy"')
      .get('.resource-page__ontologie > :nth-child(2)')
      .then((container) => cy.get(container).contains('Function analysis'));

    cy.log('Find "You might also be interested in"')
      .get('.mt-10')
      .then((container) => cy.get(container).contains('REALPHY'));
  });
});
