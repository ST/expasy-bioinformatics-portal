# Expasy Client

The client-side is based on [Nuxt.js](https://nuxtjs.org) which is a [Vue.js](https://vuejs.org)
framework.

## Styling

The project uses Tailwind as the CSS framework and styles are defined inside each components when component-specific
and inside the `/client/assets/scss/` folder when globally needed.

## Formatting and Linting

The project uses [ESLint](https://eslint.org/) and [Prettier](https://prettier.io/) for formatting
and linting the source code.

```bash
# Check the source code for formatting offenses
$ npm run prettier

# Check the source code and automatically fix formatting offenses
$ npm run prettier:fix

# Check the source code for linting offenses
$ npm run eslint

# Check the source code and automatically fix linting offenses
$ npm run eslint:fix

# Check the source code for both formatting and linting offenses
$ npm run lint
```

## Testing

The project uses [Jest](https://jestjs.io/) alongside [Vue Test Utils](https://vue-test-utils.vuejs.org/)
for testing.

```bash
# Run the tests
$ npm run test:unit

# Run the tests in watch mode
$ npm run test:unit:watch
```

### Running end to end tests

For end to end testing, this project uses [Cypress](https://www.cypress.io/).

```bash
$ npx cypress run 
```

By default, cypress is testing the production URL. You can specify another URL

```bash
$ npx cypress run --config baseUrl=https://domaine.ch
```

## Build and deploy

The production build and the deployment are automatically made by the Deployer setup. You have nothing to do manually
yourself.

## FAQ

### How to change the displayed number of databases in "{{Search}} queried in X SIB databases" ?

This is a simple string in the template, you can find it here:

[/client/pages/search/\_.vue](https://gitlab.sib.swiss/ST/expasy-bioinformatics-portal/-/blob/master/client/pages/search/_.vue#L36)

### How does the search suggestions work ?

If the user search for a word that can't retrieve and exact match, the backend does another search with fuzzy matching,
It uses the best suggestion as an exact search and returns suggestions to the user.

