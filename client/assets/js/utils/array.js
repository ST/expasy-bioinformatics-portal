/**
 * An implementation of the Durstenfeld shuffle of arrays.
 *
 * @param {array} array - Array of any values
 * @returns {array}
 */
export function shuffle(array) {
  const shuffle = [...array];

  for (let i = shuffle.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [shuffle[i], shuffle[j]] = [shuffle[j], shuffle[i]];
  }

  return shuffle;
}
