module.exports = {
  theme: {
    extend: {
      colors: {
        primary: {
          500: '#E70100',
          700: '#BA0000',
        },
        secondary: {
          300: '#5fbfed',
          500: '#009FE7',
        },
        neutral: {
          100: '#F6F6F6',
          200: '#E1E1E1',
          300: '#C5C5C5',
          500: '#9D9D9D',
          700: '#636363',
          900: '#313233',
        },
        white: '#FFF',
        black: '#000',
      },
    },
    screens: {
      'max-xs': { max: '479px' },
      sm: '480px',
      'max-sm': { max: '767px' },
      md: '768px',
      'max-md': { max: '1023px' },
      mdx: '991px',
      lg: '1024px',
      'max-lg': { max: '1279px' },
      xl: '1280px',
    },
    fontFamily: {
      heading: ['Source Sans pro', 'Arial', 'sans-serif'],
      body: ['Arial', 'sans-serif'],
    },
    fontSize: {
      xs: '0.6875rem',
      sm: '0.875rem',
      base: '1rem',
      md: '1.25rem',
      lg: '1.5rem',
      xl: '2rem',
      '2xl': '2.5rem',
    },
  },
  corePlugins: {
    container: false,
  },
};
