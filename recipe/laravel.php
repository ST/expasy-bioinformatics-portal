<?php
/**
 * This is taken from vendor/deployer/deployer/recipe/laravel.php and updated to always be in laravel project directory
 */

namespace Deployer;

set('shared_dirs', [
    'api/storage',
]);

set('writable_dirs', [
    'api/bootstrap/cache',
    'api/storage',
    'api/storage/app',
    'api/storage/app/public',
    'api/storage/framework',
    'api/storage/framework/cache',
    'api/storage/framework/sessions',
    'api/storage/framework/views',
    'api/storage/logs',
]);

set('laravel_version', static function () {
    $result = run('cd {{release_path}}/api && {{bin/php}} artisan --version');
    preg_match_all('/(\d+\.?)+/', $result, $matches);
    return $matches[0][0] ?? 5.5;
});

/**
 * Helper tasks
 */
desc('Disable maintenance mode');
task('artisan:up', static function () {
    $output = run('if [ -f {{deploy_path}}/api/current/artisan ]; then {{bin/php}} {{deploy_path}}/api/current/artisan up; fi');
    writeln('<info>' . $output . '</info>');
});

desc('Enable maintenance mode');
task('artisan:down', static function () {
    $output = run('if [ -f {{deploy_path}}/api/current/artisan ]; then {{bin/php}} {{deploy_path}}/api/current/artisan down; fi');
    writeln('<info>' . $output . '</info>');
});

desc('Execute artisan migrate');
task('artisan:migrate', static function () {
    run('{{bin/php}} {{release_path}}/api/artisan migrate --force');
})->once();

desc('Execute artisan migrate:fresh');
task('artisan:migrate:fresh', static function () {
    run('{{bin/php}} {{release_path}}/api/artisan migrate:fresh --force');
});

desc('Execute artisan migrate:rollback');
task('artisan:migrate:rollback', static function () {
    $output = run('{{bin/php}} {{release_path}}/api/artisan migrate:rollback --force');
    writeln('<info>' . $output . '</info>');
});

desc('Execute artisan migrate:status');
task('artisan:migrate:status', static function () {
    $output = run('{{bin/php}} {{release_path}}/api/artisan migrate:status');
    writeln('<info>' . $output . '</info>');
});

desc('Execute artisan db:seed');
task('artisan:db:seed', static function () {
    $output = run('{{bin/php}} {{release_path}}/api/artisan db:seed --force');
    writeln('<info>' . $output . '</info>');
});

desc('Execute artisan cache:clear');
task('artisan:cache:clear', static function () {
    run('{{bin/php}} {{release_path}}/api/artisan cache:clear');
});

desc('Execute artisan config:cache');
task('artisan:config:cache', static function () {
    run('{{bin/php}} {{release_path}}/api/artisan config:cache');
});

desc('Execute artisan route:cache');
task('artisan:route:cache', static function () {
    run('{{bin/php}} {{release_path}}/api/artisan route:cache');
});

desc('Execute artisan view:clear');
task('artisan:view:clear', static function () {
    run('{{bin/php}} {{release_path}}/api/artisan view:clear');
});

desc('Execute artisan view:cache');
task('artisan:view:cache', static function () {
    $needsVersion = 5.6;
    $currentVersion = get('laravel_version');

    if (version_compare($currentVersion, $needsVersion, '>=')) {
        run('{{bin/php}} {{release_path}}/api/artisan view:cache');
    }
});

desc('Execute artisan event:cache');
task('artisan:event:cache', static function () {
    $needsVersion = '5.8.9';
    $currentVersion = get('laravel_version');

    if (version_compare($currentVersion, $needsVersion, '>=')) {
        run('{{bin/php}} {{release_path}}/api/artisan event:cache');
    }
});

desc('Execute artisan event:clear');
task('artisan:event:clear', static function () {
    $needsVersion = '5.8.9';
    $currentVersion = get('laravel_version');

    if (version_compare($currentVersion, $needsVersion, '>=')) {
        run('{{bin/php}} {{release_path}}/api/artisan event:clear');
    }
});

desc('Execute artisan optimize');
task('artisan:optimize', static function () {
    $deprecatedVersion = 5.5;
    $readdedInVersion = 5.7;
    $currentVersion = get('laravel_version');

    if (
        version_compare($currentVersion, $deprecatedVersion, '<') ||
        version_compare($currentVersion, $readdedInVersion, '>=')
    ) {
        run('{{bin/php}} {{release_path}}/api/artisan optimize');
    }
});

desc('Execute artisan optimize:clear');
task('artisan:optimize:clear', static function () {
    $needsVersion = 5.7;
    $currentVersion = get('laravel_version');

    if (version_compare($currentVersion, $needsVersion, '>=')) {
        run('{{bin/php}} {{release_path}}/api/artisan optimize:clear');
    }
});

desc('Execute artisan queue:restart');
task('artisan:queue:restart', static function () {
    run('{{bin/php}} {{release_path}}/api/artisan queue:restart');
});

desc('Execute artisan horizon:terminate');
task('artisan:horizon:terminate', static function () {
    run('{{bin/php}} {{release_path}}/api/artisan horizon:terminate');
});

desc('Execute artisan storage:link');
task('artisan:storage:link', static function () {
    $needsVersion = 5.3;
    $currentVersion = get('laravel_version');

    if (version_compare($currentVersion, $needsVersion, '>=')) {
        run('{{bin/php}} {{release_path}}/api/artisan storage:link');
    }
});

/**
 * Task deploy:public_disk support the public disk.
 * To run this task automatically, please add below line to your deploy.php file
 *
 *     before('deploy:symlink', 'deploy:public_disk');
 *
 * @see https://laravel.com/docs/5.2/filesystem#configuration
 */
desc('Make symlink for public disk');
task('deploy:public_disk', static function () {
    // Remove from source.
    run('if [ -d $(echo {{release_path}}/api/public/storage) ]; then rm -rf {{release_path}}/api/public/storage; fi');

    // Create shared dir if it does not exist.
    run('mkdir -p {{deploy_path}}/shared/storage/app/public');

    // Symlink shared dir to release dir
    run('{{bin/symlink}} {{deploy_path}}/shared/storage/app/public {{release_path}}/api/public/storage');
});

task('laravel:nova-publish', static function () {
    run('{{bin/php}} {{release_path}}/api/artisan nova:publish');
});

desc('Build Nova custom enum field');
task('laravel:npm', static function () {
    run('cd {{release_path}}/api && npm ci && npm run prod');
    //run('cd {{release_path}}/api/nova-components/Enum && npm ci && npm run prod');
});

desc('Set up composer auth.json with Laravel Nova credentials');
task('laravel:deploy-nova-credentials', static function () {
    $environment = run('cat {{deploy_path}}/shared/api/.env');
    // Initialize variables
    $novaUsername = null;
    $novaPassword = null;

    // Split the output into lines
    $lines = explode("\n", $environment);
    foreach ($lines as $line) {
        if (strpos($line, 'NOVA_USERNAME') !== false) {
            $novaUsername = trim(substr($line, strpos($line, '=') + 1));
        }

        elseif (strpos($line, 'NOVA_PASSWORD') !== false) {
            $novaPassword = trim(substr($line, strpos($line, '=') + 1));
        }
    }
    run(sprintf('cd {{release_path}}/api && {{bin/composer}} config http-basic.nova.laravel.com %s %s',  $novaUsername , $novaPassword ));
});


