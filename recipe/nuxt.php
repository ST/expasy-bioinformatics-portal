<?php

namespace Deployer;

task('nuxt:npm-ci', static function () {
    run('cd {{release_path}}/client && npm ci');
});

task('nuxt:npm-build', static function () {
    run('cd {{release_path}}/client && npm run build');
});

task('nuxt:pm2-restart', static function () {
    run('cd {{deploy_path}}/shared/client && pm2 reload nuxt');
});
